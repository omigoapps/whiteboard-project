<?php 

include('db.php');

$validator = array('success'=> false, 'messages'=> array(), 'fav_status'=> array());

$u_id = $_POST['u_id'];
$post_id = $_POST['post_id'];

$chk_favs_tbl = mysqli_query($db, "SELECT * FROM favs_tbl");

if (!$chk_favs_tbl) {
	$create_favs_tbl = "CREATE TABLE favs_tbl (
		id INT AUTO_INCREMENT,
		u_id VARCHAR(255),
		p_id VARCHAR(255),
		fav VARCHAR(255) DEFAULT 1,
		created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	    updated_on DATETIME,
	    PRIMARY KEY (id)
	)";

	$create_query = mysqli_query($db, $create_favs_tbl);

	$chk_fav = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

	if (mysqli_num_rows($chk_fav) > 0) {

		$chk_fav_status = mysqli_fetch_assoc($chk_fav);
		if ($chk_fav_status['fav'] == 1) {
			$set_fav = mysqli_query($db, "UPDATE favs_tbl SET fav = 0 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$get_fav_status = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$fav_status = mysqli_fetch_assoc($get_fav_status);

			$validator['fav_status'] = $fav_status;
			$validator['success'] = true;
			$validator['messages'] = "Not favorite";
		} else {
			$set_fav = mysqli_query($db, "UPDATE favs_tbl SET fav = 1 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$get_fav_status = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$fav_status = mysqli_fetch_assoc($get_fav_status);

			$validator['fav_status'] = $fav_status;
			$validator['success'] = true;
			$validator['messages'] = "Set as favorite";
		}

	} else {

		$insert_fav = mysqli_query($db,"INSERT INTO favs_tbl (u_id, p_id) VALUES ('".$u_id."','".$post_id."')");

		$get_fav_status = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

		$fav_status = mysqli_fetch_assoc($get_fav_status);

		$validator['fav_status'] = $fav_status;
		$validator['success'] = true;
		$validator['messages'] = "Set as favorite";
	}
	
} else {

	$chk_fav = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

	if (mysqli_num_rows($chk_fav) > 0) {

		$chk_fav_status = mysqli_fetch_assoc($chk_fav);
		if ($chk_fav_status['fav'] == 1) {
			$set_fav = mysqli_query($db, "UPDATE favs_tbl SET fav = 0 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$get_fav_status = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$fav_status = mysqli_fetch_assoc($get_fav_status);

			$validator['fav_status'] = $fav_status;
			$validator['success'] = true;
			$validator['messages'] = "Not favorite";
		} else {
			$set_fav = mysqli_query($db, "UPDATE favs_tbl SET fav = 1 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$get_fav_status = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$fav_status = mysqli_fetch_assoc($get_fav_status);

			$validator['fav_status'] = $fav_status;
			$validator['success'] = true;
			$validator['messages'] = "Set as favorite";
		}

	} else {

		$insert_fav = mysqli_query($db,"INSERT INTO favs_tbl (u_id, p_id) VALUES ('".$u_id."','".$post_id."')");

		$get_fav_status = mysqli_query($db, "SELECT * FROM favs_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

		$fav_status = mysqli_fetch_assoc($get_fav_status);

		$validator['fav_status'] = $fav_status;
		$validator['success'] = true;
		$validator['messages'] = "Set as favorite";
	}
}

echo json_encode($validator);


?>