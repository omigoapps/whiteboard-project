<?php 

$validator = array('success'=> false, 'messages'=> array(), 'imgs'=> array());

if(isset($_POST['upload_btn'])){

	for($i=0; $i<count($_FILES['post_file']['name']); $i++) {
		$filetmp = $_FILES['post_file']['tmp_name'][$i];
		$filename = $_FILES['post_file']['name'][$i];
		$fileExt = explode('.', $filename);
		$fileActualExt = strtolower(end($fileExt));
		$file_name = uniqid().".".$fileActualExt;
		$filepath = "uploads/".$file_name;
		$upload = move_uploaded_file($filetmp, $filepath);
		
		array_push($validator['imgs'],$file_name);
	}
	
	$validator['messages'] = "Images saved in server";
	$validator['success'] = true;
}

echo json_encode($validator);

?>