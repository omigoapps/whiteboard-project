<?php
session_start();
error_reporting(E_ERROR | E_PARSE);

if(isset($_SESSION['user'])) { 
    $user_id = $_SESSION['user'];
    include 'db.php';
    if(isset($_POST['submit'])) {
        $url = $_POST['url'];
        //getting title
    function get_title($url){
      $str = file_get_contents($url);
      if(strlen($str)>0){
        $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
        preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
        return $title[1];
      }
    }

    function get_image($url){
      $page_content = file_get_contents($url);

      $dom_obj = new DOMDocument();
      $dom_obj->loadHTML($page_content);
      $meta_val = null;

      foreach($dom_obj->getElementsByTagName('meta') as $meta) {

        if($meta->getAttribute('property')=='og:image'){ 

            $meta_val = $meta->getAttribute('content');
        }
      }
      return $meta_val;
    }

    $title = get_title($url);
    $image = get_image($url);
    // Check connection
    if (mysqli_connect_errno())
      {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
      }
    if (mysqli_query($db,"INSERT INTO meta_titles (title, image, url, u_id ) 
    VALUES ('$title', '$image', '$url', '$user_id')")) {
        //echo "Done adding record";
    } else {
        echo "failed to add record";
    }
    mysqli_close($db);
    }    
    header('Location: links.php');
    
    
}
?>