<?php include('header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 m-sm-0">
            <div class="content-body">
                <div class="col-md-3" id="posts_left_block">
                    <?php include('left_blk.php'); ?>
                </div>
                <div class="col-md-6 topic-body m-sm-0" style="width: 53%;">
                    <p style="font-size: 25px;border-bottom: 1px solid #ccc;padding-bottom: 5px;"><b>View Post</b></p>
                    <div id="posts_div_blk">

                    </div>
                </div>
                <div class="col-md-2" id="posts_right_block">right block ...</div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="post_id" id="post_id" value="<?= $_GET['p_id'];  ?>">

<div class="modal fade" id="preview_post_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Download Preview</h4>
            </div>
            <div class="modal-body" id="download_blk">
                <p style="font-size: 20px;font-weight: bold;" id="pre_post_title"></p>
                <p id="pre_post_desc"></p>
                <p id="pre_topics"></p>
                <div id="pre_post_media"></div>
                <p style="margin-top: 1%;" id="pdf_btn"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script type="text/javascript" src="./assets/js/check_user_session.js"></script>
<script type="text/javascript" src="./assets/js/view_post.js"></script>
<script type="text/javascript" src="./assets/js/moment.js"></script>
<script type="text/javascript" src="./assets/js/jssocials.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
