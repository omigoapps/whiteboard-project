<?php session_start(); if(!isset($_SESSION['user'])) { include('header1.php'); ?>
    <div class="panel panel-default signup_panel">
        <div class="signup_blk"> <img src="./assets/images/logo.png" width="45" height="45">
            <h3>Sign Up</h3>
            <div class="alert alert-danger hide" id="form_err"> </div>
            <div class="alert alert-success hide" id="email_verf_msg"> <strong>Verify email: </strong>Verification email sent to your email address. </div>
            <form name="signup_form" class="signup_form">
                <div>
                    <input type="text" name="f_name" class="f_name" placeholder="First Name">
                </div>
                <div>
                    <input type="text" name="l_name" class="l_name" placeholder="Last Name">
                </div>
                <div>
                    <input type="text" name="email" class="email" placeholder="Email">
                </div>
                <div>
                    <input type="password" name="pass" class="pass" placeholder="Password">
                </div>
                <br>
                <div>
                    <button name="join_btn" type="submit" class=" btn btn-default join_btn">Join</button>
                </div>
                <br> </form>
            <div>
                <p>Already have an account? <a href="signin.php">Sign In</a></p>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/signup.js"></script>
        <?php } else { header("Location: index.php"); } ?>