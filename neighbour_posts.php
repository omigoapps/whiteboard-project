<?php session_start(); if(isset($_SESSION['user'])) { include('header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-sm-0">
                <div class="content-body">
                    <div class="col-md-3" id="posts_left_block">
                        <?php include('left_blk.php'); ?>
                    </div>
                    <div class="col-md-7 topic-body m-sm-0" style="width: 53%;">
                        <p style="font-size: 25px;border-bottom: 1px solid #e2e2e2;padding-bottom: 5px; padding-left: 15px;padding-right: 15px;"><b>Local Topics</b></p>
                        <div id="posts_div_blk"> </div>
                    </div>
                    <div class="col-md-2" id="posts_right_block"> right block ...</div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/check_user_session.js"></script>
        <script type="text/javascript" src="./assets/js/local_posts.js"></script>
        <script type="text/javascript" src="./assets/js/moment.js"></script>
        <script type="text/javascript" src="./assets/js/jssocials.min.js"></script>
        <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
        <!--<?php } else { header("Location: signin.php"); } ?>-->