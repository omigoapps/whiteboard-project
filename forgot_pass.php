<?php include('header1.php') ?>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="text-center">Create New Password</h3>
                <hr>
                <div class="alert alert-danger hide text-center" id="form_err"> </div>
                <div class="col-md-12" style="margin-top: 1%;">
                    <div class="col-md-6"> <span style="float: right;">New Password: </span> </div>
                    <div class="col-md-6">
                        <input type="password" name="new_pass" id="new_pass" placeholder="New Password" style="padding: 5px;"> </div>
                </div>
                <div class="col-md-12" style="margin-top: 1%;">
                    <div class="col-md-6"> <span style="float: right;">Confirm New Password: </span> </div>
                    <div class="col-md-6">
                        <input type="password" name="cnf_new_pass" id="cnf_new_pass" placeholder="Confirm New Password" style="padding: 5px;"> </div>
                </div>
                <input type="hidden" name="id" id="user_id" value="<?php echo $_GET['id']; ?>">
                <div class="col-md-12 text-center">
                    <hr>
                    <button id="save_new_pass" type="button" class="btn btn-default">Save Password</button>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/check_user_session.js"></script>
        <script type="text/javascript" src="./assets/js/create_new_pass.js"></script>