<?php session_start(); if (isset($_SESSION['user'])) { include('header.php'); ?>
    <div class="col-md-12 m-sm-0">
        <div class="content-body">
            <div class="col-md-3" id="posts_left_block">
                <?php include('left_blk.php'); ?>
            </div>
            <div class="col-md-6" id="create_post_blk">
                <p style="font-size: 20px;border-bottom: 1px solid #ccc;padding-bottom: 5px;"><b>Edit Post</b></p>
                <form name="edit_post_form" id="edit_post_form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="post_id" id="post_id" value="<?= $_GET['p_id']; ?>">
                    <p><b>Post Title</b></p>
                    <div>
                        <input type="text" name="post_title" id="post_title" class="form-control" placeholder="Write a simple and short post title">
                        <p id="p_title_err" class="red-color"></p>
                    </div>
                    <div id="ckeditor_blk" style="margin-top: 1%;">
                        <textarea name="editor1" id="editor1" rows="10" cols="80"> </textarea>
                        <p id="p_desc_err" class="red-color"></p>
                    </div>
                    <div class="col-md-12 m-sm-0 p-sm-0" style="margin-top: 1%;margin-bottom: 1%;">
                        <div class="col-md-7" style="margin-left: -5%;">
                            <input type="text" class="form-control" style="width: auto;display: inline-block;" name="topic" id="topic" placeholder="Topic:">
                            <button class="btn btn-default" type="button" id="add_topic">Add</button>
                        </div>
                    </div> 
                    <div style="margin-top: 1%;">
                        <input class="form-control" placeholder="Selected Topics" type="text" name="topics" id="topics"> </div>
                    <div class="col-md-12" style="padding-left: 0px;margin-top: 1%;">
                        <input type="file" name="post_file" id="post_file" multiple="multiple">
                        <button class="btn btn-default" type="button" id="add_media">Add Media <i class="fa fa-camera" aria-hidden="true"></i></button>
                    </div>
                    <div class="col-md-12" id="media_blk" style="margin-top: 1%;margin-bottom: 1%;border: 1px solid #ccc;padding: 10px;"> </div>
                    <div style="margin-top: 1%;">
                        <button class="btn btn-default" id="preview_post" type="submit">Preview</button> &nbsp;
                        <button class="btn btn-default" id="submit_post" type="submit">Update Post</button>
                    </div>
                </form>
            </div>
            <div class="col-md-3" id="posts_right_block"> right block ...</div>
        </div>
        <div class="modal fade" id="preview_post_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Preview</h4> </div>
                    <div class="modal-body">
                        <h4 id="pre_post_title"></h4>
                        <p id="pre_post_desc"></p>
                        <p id="pre_topics"></p>
                        <div id="pre_post_media"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/check_user_session.js"></script>
        <script type="text/javascript" src="./assets/js/edit_post.js"></script>
        <?php } else { header("Location: signin.php"); } ?>