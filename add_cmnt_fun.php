<?php 

include('db.php');

$validator = array('success'=> false, 'messages'=> array(),'cmnts'=> array());

$check_cmnt_tbl = mysqli_query($db, "SELECT * FROM cmnts_tbl");

if (!$check_cmnt_tbl) {
	$create_cmnts_tbl = "CREATE TABLE cmnts_tbl (
		id INT AUTO_INCREMENT,
		u_id VARCHAR(255),
		u_name VARCHAR(255),
		p_id VARCHAR(255),
		likes INT DEFAULT 0,
		dislikes INT DEFAULT 0,
		cmnt VARCHAR(255),
		created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	    updated_on DATETIME,
	    PRIMARY KEY (id)
	)";

	mysqli_query($db, $create_cmnts_tbl);

	$save_cmnt = mysqli_query($db, "INSERT INTO cmnts_tbl (u_id, u_name, p_id, cmnt) VALUES 
		('".$_POST['u_id']."','".$_POST['u_name']."','".$_POST['p_id']."','".$_POST['cmnt']."') ");
	if ($save_cmnt) {

		$get_comments = mysqli_query($db, "SELECT * FROM cmnts_tbl WHERE p_id = '".$_POST['p_id']."'");
		while ($row = mysqli_fetch_assoc($get_comments)) {
			array_push($validator['cmnts'], $row);
		}

		$validator['success'] = true;
		$validator['messages'] = "Comments retrieved";
	}
} else {

	$save_cmnt = mysqli_query($db, "INSERT INTO cmnts_tbl (u_id, u_name, p_id, cmnt) VALUES 
		('".$_POST['u_id']."','".$_POST['u_name']."','".$_POST['p_id']."','".$_POST['cmnt']."') ");
	if ($save_cmnt) {
		$get_comments = mysqli_query($db, "SELECT * FROM cmnts_tbl WHERE p_id = '".$_POST['p_id']."'");
		while ($row = mysqli_fetch_assoc($get_comments)) {
			array_push($validator['cmnts'], $row);
		}

		$validator['success'] = true;
		$validator['messages'] = "Comments retrieved";
	}
}

echo json_encode($validator);

?>