<script src="https://use.fontawesome.com/releases/v5.2.0/js/all.js" data-auto-replace-svg="nest"></script>
<script type="text/javascript" src="./assets/js/jquery.js"></script>
<script type="text/javascript" src="./assets/js/bootstrap.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
                 $(".menu_toggle").click(function () {
                     $(".search-wrapper").slideToggle();
                 })
             })
//             Get current path and find target link
         var path = window.location.pathname.split("/").pop();
         // Account for home page with empty path
         if (path == '') {
             path = 'index.php';
         }
         var target = $('.nav-main a[href="' + path + '"]');
         // Add active class to target link
         target.addClass('tab_active');
</script>
</body>

</html>