<ul class="left_blk_ul">
	<li><a href="#"><i class="fas fa-desktop"></i> Dashboard</a></li>
	<li><a id="neighbour_posts_li" href="neighbour_posts.php"><i class="fas fa-home"></i> Neighbour Posts</a></li>
	<li><a id="recent_topics_li" class="tab_active" href="#"><i class="fas fa-history"></i> Recent Topics</a></li>
	<li>
		<ul class="left_blk_inner_ul">
			<li><a href="#">Technology</a></li>
		    <li><a href="#">Entertainment</a></li>
		    <li><a href="#">Political</a></li>
		    <li><a href="#">Social</a></li>
		</ul>
	</li>
</ul>
<a style="font-size: 17px;margin-left: 2%;" href="">More</a>
<script type="text/javascript">
	$(document).ready(function(){
		$(".left_blk_ul li a").click(function() {
			$(this).addClass('tab_active');
			$(".left_blk_ul li a").not(this).removeClass('tab_active');
		});

		var url = window.location.pathname;
        if (url.indexOf('neighbour_posts') >= 0) {
        	$(".left_blk_ul li a").removeClass('tab_active');
        	$("#neighbour_posts_li").addClass('tab_active');
        }
	});
</script>