<?php 

include('db.php');

$validator = array('success'=> false, 'messages'=> array(), 'dis_like_status'=> array(), 'dis_likes'=> '');

$u_id = $_POST['u_id'];
$post_id = $_POST['post_id'];

$chk_dis_liked_tbl = mysqli_query($db, "SELECT * FROM dis_liked_tbl");

if (!$chk_dis_liked_tbl) {
	$create_dis_liked_tbl = "CREATE TABLE dis_liked_tbl (
		id INT AUTO_INCREMENT,
		u_id VARCHAR(255),
		p_id VARCHAR(255),
		liked VARCHAR(255) DEFAULT 1,
		created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	    updated_on DATETIME,
	    PRIMARY KEY (id)
	)";

	$create_query = mysqli_query($db, $create_dis_liked_tbl);

	$chk_user = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

	if (mysqli_num_rows($chk_user) > 0) {

		$chk_dis_like_status = mysqli_fetch_assoc($chk_user);
		if ($chk_dis_like_status['liked'] == 1) {
			$dis_like = mysqli_query($db, "UPDATE dis_liked_tbl SET liked = 0 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");
			$get_pre_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_dis_likes_s = mysqli_fetch_assoc($get_pre_dis_likes);

			$pre_dis_likes = (int)$pre_dis_likes_s['dis_likes'];

			$pre_dis_likes = $pre_dis_likes -1;

			$update_dis_likes = mysqli_query($db,"UPDATE posts_tbl SET dis_likes = '".$pre_dis_likes."' WHERE id = '".$post_id."'");

			$get_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_dis_like_status = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$dis_like_status = mysqli_fetch_assoc($get_dis_like_status);

			$dis_likes = mysqli_fetch_assoc($get_dis_likes);

			$validator['dis_likes'] = $dis_likes['dis_likes'];
			$validator['dis_like_status'] = $dis_like_status;
			$validator['success'] = true;
			$validator['messages'] = "Not Disliked";
		} else {
			$dis_like = mysqli_query($db, "UPDATE dis_liked_tbl SET liked = 1 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");
			$get_pre_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_dis_likes_s = mysqli_fetch_assoc($get_pre_dis_likes);

			$pre_dis_likes = (int)$pre_dis_likes_s['dis_likes'];

			$pre_dis_likes = $pre_dis_likes +1;

			$update_dis_likes = mysqli_query($db,"UPDATE posts_tbl SET dis_likes = '".$pre_dis_likes."' WHERE id = '".$post_id."'");

			$get_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_dis_like_status = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$dis_like_status = mysqli_fetch_assoc($get_dis_like_status);

			$dis_likes = mysqli_fetch_assoc($get_dis_likes);

			$validator['dis_likes'] = $dis_likes['dis_likes'];
			$validator['dis_like_status'] = $dis_like_status;
			$validator['success'] = true;
			$validator['messages'] = "Disliked";
		}

	} else {

		$insert_dis_liked = mysqli_query($db,"INSERT INTO dis_liked_tbl (u_id, p_id) VALUES ('".$u_id."','".$post_id."')");

		$get_pre_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = ".$post_id."");

		$pre_dis_likes_s = mysqli_fetch_assoc($get_pre_dis_likes);

		$pre_dis_likes = (int)$pre_dis_likes_s['likes'];

		$pre_dis_likes = $pre_dis_likes +1;

		$update_likes = mysqli_query($db,"UPDATE posts_tbl SET dis_likes = '".$pre_dis_likes."' WHERE id = '".$post_id."'");

		$get_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = '".$post_id."'");

		$dis_likes = mysqli_fetch_assoc($get_dis_likes);

		$get_dis_like_status = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

		$dis_like_status = mysqli_fetch_assoc($get_dis_like_status);

		$validator['success'] = true;
		$validator['dis_like_status'] = $dis_like_status;
		$validator['dis_likes'] = $dis_likes['dis_likes'];
		$validator['messages'] = "Disliked";
	}
	
} else {

	$chk_user = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

	if (mysqli_num_rows($chk_user) > 0) {

		$chk_dis_like_status = mysqli_fetch_assoc($chk_user);
		if ($chk_dis_like_status['liked'] == 1) {
			$dis_like = mysqli_query($db, "UPDATE dis_liked_tbl SET liked = 0 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");
			$get_pre_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_dis_likes_s = mysqli_fetch_assoc($get_pre_dis_likes);

			$pre_dis_likes = (int)$pre_dis_likes_s['dis_likes'];

			$pre_dis_likes = $pre_dis_likes -1;

			$update_dis_likes = mysqli_query($db,"UPDATE posts_tbl SET dis_likes = '".$pre_dis_likes."' WHERE id = '".$post_id."'");

			$get_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_dis_like_status = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$dis_like_status = mysqli_fetch_assoc($get_dis_like_status);

			$dis_likes = mysqli_fetch_assoc($get_dis_likes);

			$validator['dis_likes'] = $dis_likes['dis_likes'];
			$validator['dis_like_status'] = $dis_like_status;
			$validator['success'] = true;
			$validator['messages'] = "Not Disliked";
		} else {
			$dis_like = mysqli_query($db, "UPDATE dis_liked_tbl SET liked = 1 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");
			$get_pre_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_dis_likes_s = mysqli_fetch_assoc($get_pre_dis_likes);

			$pre_dis_likes = (int)$pre_dis_likes_s['dis_likes'];

			$pre_dis_likes = $pre_dis_likes +1;

			$update_dis_likes = mysqli_query($db,"UPDATE posts_tbl SET dis_likes = '".$pre_dis_likes."' WHERE id = '".$post_id."'");

			$get_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_dis_like_status = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$dis_like_status = mysqli_fetch_assoc($get_dis_like_status);

			$dis_likes = mysqli_fetch_assoc($get_dis_likes);

			$validator['dis_likes'] = $dis_likes['dis_likes'];
			$validator['dis_like_status'] = $dis_like_status;
			$validator['success'] = true;
			$validator['messages'] = "Disliked";
		}

	} else {

		$insert_dis_liked = mysqli_query($db,"INSERT INTO dis_liked_tbl (u_id, p_id) VALUES ('".$u_id."','".$post_id."')");

		$get_pre_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = ".$post_id."");

		$pre_dis_likes_s = mysqli_fetch_assoc($get_pre_dis_likes);

		$pre_dis_likes = (int)$pre_dis_likes_s['dis_likes'];

		$pre_dis_likes = $pre_dis_likes +1;

		$update_likes = mysqli_query($db,"UPDATE posts_tbl SET dis_likes = '".$pre_dis_likes."' WHERE id = '".$post_id."'");

		$get_dis_likes = mysqli_query($db, "SELECT dis_likes FROM posts_tbl WHERE id = '".$post_id."'");

		$dis_likes = mysqli_fetch_assoc($get_dis_likes);

		$get_dis_like_status = mysqli_query($db, "SELECT * FROM dis_liked_tbl WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

		$dis_like_status = mysqli_fetch_assoc($get_dis_like_status);

		$validator['success'] = true;
		$validator['dis_like_status'] = $dis_like_status;
		$validator['dis_likes'] = $dis_likes['dis_likes'];
		$validator['messages'] = "Disliked";
	}
}

echo json_encode($validator);


?>