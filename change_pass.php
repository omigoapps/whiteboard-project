<?php session_start(); if (isset($_SESSION['user'])) { include('header.php') ?>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<h3 class="text-center">Change Password</h3><hr>
			<div class="alert alert-danger hide text-center" id="form_err">
			  
			</div>
			<form name="change_pass" id="change_pass_form" method="post">
				<div class="col-md-12 form-group">
					<div>
						<span>Old Password: </span>
					</div>
					<div>
						<input type="password" name="old_pass" id="old_pass" placeholder="Old Password" style="padding: 5px;">
					</div>
				</div>
				<div class="col-md-12 form-group" style="margin-top: 1%;">
					<div>
						<span>New Password: </span>
					</div>
					<div>
						<input type="password" name="new_pass" id="new_pass" placeholder="New Password" style="padding: 5px;">
					</div>
				</div>
				<div class="col-md-12 form-group" style="margin-top: 1%;">
					<div>
						<span>Confirm New Password: </span>
					</div>
					<div>
						<input type="password" name="cnf_new_pass" id="cnf_new_pass" placeholder="Confirm New Password" style="padding: 5px;">
					</div>
				</div>
				<div class="col-md-12 text-center">
					<hr>
					<button id="change_pass" class="btn btn-primary">Change</button>
					<a href="user_profile.php" class="btn btn-default">Cancel</a>
				</div>
			</form>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>
<script type="text/javascript" src="./assets/js/check_user_session.js"></script>
<script type="text/javascript" src="./assets/js/change_pass.js"></script>
<?php } else { header("Location: signin.php"); } ?>