var posts_data = '';
var cmnts_data = '';
var posts_arr = [];
$(document).ready(function(){
    getFavData();

    


});

function onLike(post_id) {

    // $("#likes_"+ post_id).toggleClass('like_active');
    var user = JSON.parse(localStorage.getItem('userObj'));

    $.ajax({
        url:'like_post_fun.php',
        type:'post',
        data:{u_id:user.id, post_id:post_id},
        dataType:'json',
        success:function(res) {
            console.log(res);
            if (res.success == true) {
                
                if (res.like_status.liked == '1') {
                    $("#likes_"+ post_id).addClass('like_active');
                } else {
                    $("#likes_"+ post_id).removeClass('like_active');
                }

                $('#likes_tag_'+ post_id).text(res.likes);

            }
        },
        error:function(err) {
            console.log('error',err)
        }
    })
}

function onDisLike(post_id) {

    // $("#dis_likes_"+ post_id).toggleClass('dis_like_active');
    var user = JSON.parse(localStorage.getItem('userObj'));

    $.ajax({
        url:'dislike_post_fun.php',
        type:'post',
        data:{u_id:user.id, post_id:post_id},
        dataType:'json',
        success:function(res) {
            console.log(res);
            if (res.success == true) {

                if (res.dis_like_status.liked == '1') {
                    $("#dis_likes_"+ post_id).addClass('dis_like_active');
                } else {
                    $("#dis_likes_"+ post_id).removeClass('dis_like_active');
                }

                $('#dis_likes_tag_'+ post_id).text(res.dis_likes);
            }
        },
        error:function(err) {
            console.log('error',err)
        }
    })
}

function onFav(post_id) {

    $("#fav_"+ post_id).toggleClass('fav_active');
    var user = JSON.parse(localStorage.getItem('userObj'));

    $.ajax({
        url:'fav_posts_fun.php',
        type:'post',
        data:{u_id:user.id, post_id:post_id},
        dataType:'json',
        success:function(res) {
            console.log(res);
            if (res.success == true) {
                // if (res.fav_status.fav == '1') {
                //  $("#fav_"+ post_id).addClass('fav_active');
                // } else {
                //  $("#fav_"+ post_id).removeClass('fav_active');
                // }
            }
        },
        error:function(err) {
            console.log('error',err)
        }
    })
}

function getFavData() {
    var user = JSON.parse(localStorage.getItem('userObj'));
    $("#posts_div_blk").html('<p>Loading....</p>');

    $.ajax({
        url:'get_fav_data_fun.php',
        type:'post',
        data:{u_id:user.id},
        dataType:'json',
        success:function(res) {
            console.log(res);
            if (res.success == true) {
                posts_arr = res.posts;
                if (res.posts.length > 0) {
                    for (var i = 0; i < res.posts.length; i++) {
                        if (res.posts[i].u_id == res.user.id) {
                            var profile_pic = res.user.profile_pic == null ? 'profile.png' : res.user.profile_pic;
                        } else {
                            var profile_pic = res.posts[i].u_image == null ? 'profile.png' : res.posts[i].u_image;
                        }
                        
                        var profile_link = "user_profile.php?id=" + res.posts[i].u_id;
                        // var filesArray = res.posts[i].files.replace("[","").replace("]","").split(',');
                        posts_data += '<div class="posts_div" id="posts_div_'+ res.posts[i].id +'"><div><span class="p_topics">Related Topics: '+ res.posts[i].topics +'</span>'+
                                '<span style="display:none;float: right;" id="edit_del_blk_'+ res.posts[i].id +'">'+
                                    '<a href="edit_post.php?p_id='+ res.posts[i].id +'&u_id='+ res.posts[i].u_id +'">'+
                                        '<span class="glyphicon glyphicon-edit glp_edit"></span></a>&nbsp;'+
                                    '<span class="glyphicon glyphicon-trash glp_delete" onclick="onDelPost('+ res.posts[i].id +')"></span></span></div>'+
                        '<div class="posted_by col-md-12" style="margin-bottom: 1%;margin-top: 1%;">'+
                            '<div class="col-md-3" style="margin-left: -5%;">'+
                                '<img class="img-profile" src="./uploads/'+ profile_pic +'" width="50%"></div>'+
                            '<div class="col-md-6" style="margin-left: -13%;"><a href="'+ profile_link +'"><span>'+ res.posts[i].u_name +'</span></a><br><span class="posted_date">'+ moment(res.posts[i].created_on).fromNow() +'</span></div></div>'+
                        '<div style="margin-top: 1%;">'+
                            '<a href="view_post.php?p_id='+ res.posts[i].id +'" ><span class="post_title"><b>'+ res.posts[i].p_title +'</b></span></a></div>'+
                        '<div class="col-md-12">'+
                            '<div class="col-md-9" id="post_desc_blk_'+ res.posts[i].id +'" style="margin-left: -5%;">'+
                                '<p style="font-size: 16px;">'+ res.posts[i].p_desc +'</p></div>'+
                            '<div class="col-md-3 attch_blk" id="post_img_blk_'+ res.posts[i].id +'">'+
                                '<img onclick="onDownload('+ res.posts[i].id +')" src="./assets/images/image_icon.png" width="100%" id="attach_img_'+ res.posts[i].id +'"></div></div>'+
                        '<div style="margin-top: 1%;">'+
                            '<span onclick="onLike('+ res.posts[i].id +')">'+
                                '<i class="fa home-fa-icons fa-thumbs-up " id="likes_'+ res.posts[i].id +'"></i></span>'+
                            '<span onclick="onDisLike('+ res.posts[i].id +')">'+
                                '<i class="fa home-fa-icons fa-thumbs-down " id="dis_likes_'+ res.posts[i].id +'"></i></span>'+
                            '<span onclick="onFav('+ res.posts[i].id +')">'+
                            '<i class="fa home-fa-icons fa-star " id="fav_'+ res.posts[i].id +'"></i></span>'+
                            '<span style="margin-left: 67%;">'+
                            '<span onclick="onDownload('+ res.posts[i].id +')">'+
                                '<i class="fa home-fa-icons fa-download" ></i></span>'+
                            '<span onclick="onShare('+ res.posts[i].id +')">'+
                                '<i class="fa home-fa-icons fa-share-alt" id="share_'+ res.posts[i].id +'"></i></span>'+
                            '<span onclick="clickComment('+ res.posts[i].id +')">'+
                                '<i class="fa home-fa-icons fa-comment"></i></span></span></div>'+
                        '<div style="margin-top: 1%;"><span id="likes_tag_'+ res.posts[i].id +'">'+ res.posts[i].likes +'</span> Likes &nbsp;'+ 
                            '<span id="dis_likes_tag_'+ res.posts[i].id +'">'+ res.posts[i].dis_likes +'</span> Dislikes'+
                            '<span class="pull-right cmnt_tag" onclick="clickComment('+ res.posts[i].id +')">Comments<span id="cmnts_count_'+res.posts[i].id +'"></span></span></div>'+
                            '<div id="jssocials_id_'+res.posts[i].id +'" style="display:none;"></div>'+
                            '<div style="display:none;" class="cmnts_blk" id="cmnts_blk_'+res.posts[i].id +'">'+
                            '<div><ul class="cmnts_ul" id="cmnts_ul_'+res.posts[i].id +'"></ul></div><div id="editor_blk_'+ res.posts[i].id +'"><textarea name="editor" id="editor_'+res.posts[i].id +'" rows="10" cols="80"></textarea>'+
                            '<div style="margin-top:5px;"><button type="button" class="btn btn-default btn-sm" onclick="onComment('+res.posts[i].id +')">ADD COMMENT</button></div></div></div></div>';


                    }

                    $("#posts_div_blk").html(posts_data);

                    for (var i = 0; i < posts_arr.length; i++) {
                        if (posts_arr[i].u_id == res.user.id) {
                            $("#edit_del_blk_"+ posts_arr[i].id).show();
                        }

                        CKEDITOR.replace( 'editor_'+posts_arr[i].id );

                        for (var j = 0; j < res.like_status.length; j++) {
                            if (res.user.id == res.like_status[j].u_id && posts_arr[i].id == res.like_status[j].p_id) {
                                if (res.like_status[j].liked == 1) {
                                    $("#likes_"+ posts_arr[i].id).addClass("like_active");
                                }
                            }
                        }

                        var filesArray = posts_arr[i].files.replace("[","").replace("]","").split(',');
                        
                        if (filesArray[0] == "") {
                            $("#post_img_blk_"+posts_arr[i].id).remove()
                            $("#post_desc_blk_"+posts_arr[i].id).css("width","100%");
                        } else {
                            var fileExt = filesArray[0].split('.').pop();
                            if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif') {
                                $("#attach_img_"+ posts_arr[i].id).attr('src','uploads/'+ filesArray[0]);
                            }
                        }

                        cmnts_data = '';
                        for (var j = 0; j < res.cmnts.length; j++) {
                            if (posts_arr[i].id == res.cmnts[j].p_id) {
                                cmnts_data += "<li><div class='cmnt_div'><span class='cmnt_user'>"+ res.cmnts[j].u_name +"</span>&nbsp;&nbsp;<span style='opacity:0.8'>"+ res.cmnts[j].cmnt +"</span></div><div class='like_dislike_cmnt'><span id='like_cmnt_"+ res.cmnts[j].id +"' onclick='likeCmnt("+ res.cmnts[j].id +")'><i class='fa home-fa-icons fa-thumbs-up'></i></span>&nbsp;&nbsp;&nbsp;<span onclick='disLikeCmnt("+ res.cmnts[j].id +")' id='dislike_cmnt_"+ res.cmnts[j].id +"'><i class='fa home-fa-icons fa-thumbs-down'></i></span></div></li>"
                                $("#cmnts_ul_"+posts_arr[i].id).html(cmnts_data);
                                var li_length = $("#cmnts_ul_"+posts_arr[i].id+" li").length;
                                $("#cmnts_count_"+ posts_arr[i].id).text('('+li_length+')');
                            }
                        }

                        for (var j = 0; j < res.cmnts.length; j++) {
                            if (res.user.id == res.cmnts[j].u_id) {
                                if(res.cmnts[j].likes == 1) {
                                    $("#like_cmnt_"+ res.cmnts[j].id +' i').addClass('like_dis_active');
                                } else {
                                    $("#like_cmnt_"+ res.cmnts[j].id +' i').removeClass('like_dis_active');
                                }
                                if(res.cmnts[j].dislikes == 1) {
                                    $("#dislike_cmnt_"+ res.cmnts[j].id +' i').addClass('like_dis_active');
                                } else {
                                    $("#dislike_cmnt_"+ res.cmnts[j].id +' i').removeClass('like_dis_active');
                                }
                            }
                        }
                    }

                    for (var i = 0; i < posts_arr.length; i++) {
                        for (var j = 0; j < res.dis_like_status.length; j++) {
                            if (res.user.id == res.dis_like_status[j].u_id && posts_arr[i].id == res.dis_like_status[j].p_id) {
                                if (res.dis_like_status[j].liked == 1) {
                                    $("#dis_likes_"+ posts_arr[i].id).addClass("dis_like_active");
                                }
                            }
                        }
                    }

                    for (var i = 0; i < posts_arr.length; i++) {
                        for (var j = 0; j < res.fav_status.length; j++) {
                            if (res.user.id == res.fav_status[j].u_id && posts_arr[i].id == res.fav_status[j].p_id) {
                                if (res.fav_status[j].fav == 1) {
                                    $("#fav_"+ posts_arr[i].id).addClass("fav_active");
                                }
                            }
                        }
                    }

                    for (var i = 0; i < posts_arr.length; i++) {
                        $("#jssocials_id_"+ posts_arr[i].id).jsSocials({
                            showLabel: false,
                            shareIn:"popup",
                            shares: [{
                                share: "facebook",
                                logo: "./assets/images/fb.png"
                            }, {
                                share: "twitter",
                                logo: "./assets/images/twitter.png"
                            }, {
                                share: "whatsapp",
                                logo: "./assets/images/watsapp.png"
                            }, {
                                share: "googleplus",
                                logo: "./assets/images/google.png"
                            }, {
                                share: "linkedin",
                                logo: "./assets/images/linkedin.png"
                            }]
                        });
                    }

                } else {
                    $("#posts_div_blk").html('<p>No favorite posts avaliable</p>');
                }
            } else {
                $("#posts_div_blk").html('<p>No favorite posts avaliable</p>');
            }
        },
        error:function(err) {
            console.log('error',err)
        }
    })
}

function onShare(id) {
    $("#jssocials_id_"+id).toggle(500);
    $("#share_"+id).toggleClass("share_active");
}

function onDownload(id) {
    var post = posts_arr.find(item => item.id == id);
    var filesArray = post.files.replace("[","").replace("]","").split(',');

     if (filesArray.length > 0) {
        var imgdata = '';
        for(i=0; i< filesArray.length; i++)
        {
            var fileExt = filesArray[i].split('.').pop();
            if (fileExt == 'mp3' || fileExt == 'wav') {
                imgdata += '<a href="'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> Audio</span></a>';
            }

            if (fileExt == 'mp4' || fileExt == '3gp') {
                imgdata += '<a href="'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> Video</span></a>';
            }
            if (fileExt == 'pdf' || fileExt == 'doc' || fileExt == 'txt') {
                imgdata += '<a href="'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> File</span></a>';
            }
            if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif') {
                imgdata += '<a href="'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> Image</span></a>';
            }
        }
        $('#pre_post_media').html(imgdata);
        $('#pre_post_title').text(post.p_title);
        $('#pre_post_desc').html(post.p_desc);
        $('#pre_topics').text(post.topics);
        $('#pdf_btn').html('<span class="badge attch_badge" onclick="downloadPost('+ id +')">Download Post</span>');
        $("#preview_post_modal").modal();
     } else {
        $('#pre_post_media').html('No Media Avaliable to Download');
        $('#pre_post_title').text(post.p_title);
        $('#pre_post_desc').html(post.p_desc);
        $('#pre_topics').text(post.topics);
        $('#pdf_btn').html('<span class="badge attch_badge" onclick="downloadPost('+ id +')">Download Post</span>');
        $("#preview_post_modal").modal();
     }
}

function clickComment(id) {
    $("#cmnts_blk_"+id).toggle(500);
    $("#editor_blk_"+id).show(500);
}

function onDelPost(post_id) {
     if(confirm("Do you really want to delete this post?")) {
        $.ajax({
            url:'del_post_fun.php',
            type:'post',
            data:{post_id:post_id},
            dataType:'json',
            success:function(res) {
                console.log(res);
                if (res.success == true) {
                    posts_data = '';
                    getAllData();
                }
            },
            error:function(err) {
                console.log('error',err)
            }
        })
     }
}

function onComment(post_id) {

    $("#editor_blk_"+post_id).hide(500);

    var id = 'editor_'+post_id;
    var user = JSON.parse(localStorage.getItem('userObj'));

    var content = CKEDITOR.instances[id].getData();
    var cmnt = content.replace(/<\/?[^>]+(>|$)/g, "");

    $.ajax({
        url:'add_cmnt_fun.php',
        type:'post',
        data:{u_id:user.id,u_name:user.fname, p_id:post_id,cmnt:cmnt},
        dataType:'json',
        success:function(res) {
            console.log(res);
            if (res.success == true) {
                var add_cmnts_data = '';
                if (res.cmnts.length > 0) {
                    for (var j = 0; j < res.cmnts.length; j++) {
                        add_cmnts_data += "<li><div class='cmnt_div'><span class='cmnt_user'>"+ res.cmnts[j].u_name +"</span>&nbsp;&nbsp;<span style='opacity:0.8'>"+ res.cmnts[j].cmnt +"</span></div><div class='like_dislike_cmnt'><span id='like_cmnt_"+ res.cmnts[j].id +"' onclick='likeCmnt("+ res.cmnts[j].id +")'><i class='fa home-fa-icons fa-thumbs-up'></i></span>&nbsp;&nbsp;&nbsp;<span onclick='disLikeCmnt("+ res.cmnts[j].id +")' id='dislike_cmnt_"+ res.cmnts[j].id +"'><i class='fa home-fa-icons fa-thumbs-down'></i></span></div></li>"
                        $("#cmnts_ul_"+post_id).html(add_cmnts_data);
                        var li_length = $("#cmnts_ul_"+post_id+" li").length;
                        $("#cmnts_count_"+ post_id).text('('+li_length+')');
                    }
                }
                
            }
        },
        error:function(err) {
            console.log('error',err)
        }
    })
}

function onCmntIcon(post_id) {
    $("#editor_blk_"+post_id).toggle(500);
}

function likeCmnt(cmnt_id) {
    if (localStorage.userObj) {
        var tag = "Like";
        $.ajax({
            url:'like_dis_cmnt_fun.php',
            type:'post',
            data:{tag:tag, cmnt_id:cmnt_id},
            dataType:'json',
            success:function(res) {
                console.log(res);
                if (res.success == true) {
                    if(res.like_dis.likes == 1) {
                        $("#like_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#like_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                    if(res.like_dis.dislikes == 1) {
                        $("#dislike_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#dislike_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                }
            },
            error:function(err) {
                console.log('error',err)
            }
        })
    } else {
        window.location.href = 'signin.php';
    }
}

function disLikeCmnt(cmnt_id) {
    if (localStorage.userObj) {
        var tag = "Dislike";
        $.ajax({
            url:'like_dis_cmnt_fun.php',
            type:'post',
            data:{tag:tag, cmnt_id:cmnt_id},
            dataType:'json',
            success:function(res) {
                console.log(res);
                if (res.success == true) {
                    if(res.like_dis.likes == 1) {
                        $("#like_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#like_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                    if(res.like_dis.dislikes == 1) {
                        $("#dislike_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#dislike_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                }
            },
            error:function(err) {
                console.log('error',err)
            }
        })
    } else {
        window.location.href = 'signin.php';
    }
}

