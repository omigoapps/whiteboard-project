$(document).ready(function(){
    $('#reset_pass').click(function() {
      var email = $('#rec_email').val();

      if (email == '') {
        $('#form_err').removeClass('hide');
        $('#form_err').text('Recovery email required');
        return false;
      }

      if (email != '') {
        $.ajax({
          url:'pass_rec_mail.php',
          type:'post',
          data:{email:email},
          dataType:'json',
          success:function(res) {
            console.log(res);
            if (res.success == true) {
              $('#form_err').removeClass('hide');
              $('#form_err').removeClass('alert-danger');
              $('#form_err').addClass('alert-success');
              $('#form_err').text(res.messages);
            } else {
              $('#form_err').removeClass('hide');
              $('#form_err').text(res.messages);
            }
          },
          error:function(err) {
            console.log('error',err)
          }
        })
        return false;
      }
    })
});