$(document).ready(function(){
	if (localStorage.userObj) {

    $('#change_pass').click(function() {
    	var old_pass = $('#old_pass').val();
    	var new_pass = $('#new_pass').val();
    	var cnf_new_pass = $('#cnf_new_pass').val();
    	var user = JSON.parse(localStorage.getItem('userObj'));
    	var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    	if (old_pass == '') {
		 		$('#form_err').removeClass('hide');
		 		$('#form_err').text('Old Password required');
		 		return false;
		 	}
		 	if (new_pass == '') {
		 		$('#form_err').removeClass('hide');
		 		$('#form_err').text('New Password required');
		 		return false;
		 	}
		 	if(!strongRegex.test(new_pass)) {
		 		$('#form_err').removeClass('hide');
		 		$('#form_err').text('Password should contain min 8 characters with atleast 1 special character, 1 number, uppercase and lowercase letters');
		 		return false;
		 	}
		 	if (cnf_new_pass == '') {
		 		$('#form_err').removeClass('hide');
		 		$('#form_err').text('Confirm Password required');
		 		return false;
		 	}
		 	if (new_pass != cnf_new_pass) {
		 		$('#form_err').removeClass('hide');
		 		$('#form_err').text('Both New & Confirm Passwords should be same');
		 		return false;
		 	}

		 	if (old_pass != '' && new_pass != '' && cnf_new_pass != '' && new_pass == cnf_new_pass && strongRegex.test(new_pass)) {
		 		$.ajax({
			 		url:'change_pass_fun.php',
			 		type:'post',
			 		data:{old_pass:old_pass,new_pass:new_pass,id:user.id},
			 		dataType:'json',
			 		success:function(res) {
			 			console.log(res);
			 			if (res.success == true) {
			 				$('#form_err').removeClass('hide');
			 				$('#form_err').removeClass('alert-danger');
			 				$('#form_err').addClass('alert-success');
			 				$('#form_err').text(res.messages+'. Redirecting to login page...');
			 				setTimeout(function () {
                window.location.href = 'signin.php';
              }, 2000);
			 			} else {
			 				$('#form_err').removeClass('hide');
			 				$('#form_err').text(res.messages);
			 			}
			 		},
			 		error:function(err) {
			 			console.log('error',err)
			 		}
			 	})
		 		return false;
		 	}


    })
    
  }
});