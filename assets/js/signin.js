$(document).ready(function(){
	$('.signin_btn').click(function() {
		var data = $('.signin_form').serialize();

		var email = $('.email').val();
 		var pass = $('.pass').val();

 		if (email == '') {
	 		$('#form_err').removeClass('hide');
	 		$('#form_err').text('Email required');
	 		return false;
	 	}
	 	if (pass == '') {
	 		$('#form_err').removeClass('hide');
	 		$('#form_err').text('Password required');
	 		return false;
	 	}

	 	if (email != '' && pass != '') {

	 		$.ajax({
		 		url:'signin_fun.php',
		 		type:'post',
		 		data:data,
		 		dataType:'json',
		 		success:function(res) {
		 			console.log(res);
		 			if (res.success == true) {
		 				$('.signin_form')[0].reset();
		 				localStorage.setItem('userObj', JSON.stringify(res.user));
		 				var user = JSON.parse(localStorage.getItem('userObj'));
		 				window.location.href = 'index.php?id='+ user.id +'';
		 			} else {
		 				$('#form_err').removeClass('hide');
		 				$('#form_err').text(res.messages);
		 			}
		 		},
		 		error:function(err) {
		 			console.log('error',err)
		 		}
		 	})
	 		return false;
 	}

	})
});