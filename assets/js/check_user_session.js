$(document).ready(function(){
  if (localStorage.userObj) {
    var user = JSON.parse(localStorage.getItem('userObj'));
    
     // $('.nav_elems2').addClass('hide');
     // $('.nav_elems3').removeClass('hide');
     $('#username').text(user.fname);

      $('#signout').click(function() {
        $.ajax({
          url:'logout.php',
          type:'get',
          dataType:'json',
          success:function(res) {
            console.log(res);
            if (res.success == true) {
              localStorage.clear();
              window.location.href = 'signin.php';
            }
          },
          error:function(err) {
            console.log('error',err)
          }
        })
      })
      $('#my_profile').click(function() {
        var user = JSON.parse(localStorage.getItem('userObj'));
        window.location.href = 'user_profile.php?id='+ user.id +'';
      })
  }
	
});