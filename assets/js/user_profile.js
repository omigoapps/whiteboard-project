$(document).ready(function(){
	var user = JSON.parse(localStorage.getItem('userObj'));
	$('#u_name').text(user.fname);
	$('#u_email').text(user.email);
	$('#u_mobile').text(user.mobile);
	$('#u_about').text(user.about);
	$('#u_city').text(user.city);
	$('#u_country').text(user.country);
	if (user.profile_pic != null) {
		$('#upload_pic').attr('src','./uploads/'+user.profile_pic+'');
	}
	
	$('#edit_profile').click(function() {
		window.location.href = 'edit_profile.php?id='+ user.id +'';
	});
});