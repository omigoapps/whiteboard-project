var posts_data = '';
var cmnts_data = '';
var posts_arr = [];
$(document).ready(function(){
	getPostData();

});


function getPostData() {
	var u_id = 0;
	var user = JSON.parse(localStorage.getItem('userObj'));
	if (user != null) {
		u_id = user.id;
	}
	let post_id = $("#post_id").val();
	$("#posts_div_blk").html('<p>Loading....</p>');

	$.ajax({
 		url:'get_post_full_fun.php',
 		type:'post',
 		data:{u_id:u_id, post_id:post_id},
 		dataType:'json',
 		success:function(res) {
 			console.log(res);
 			if (res.success == true) {
 				posts_arr = [];
 				posts_arr.push(res.post);
 				if (res.user != null) {
 					u_id = res.user.id;
 				}
				if (res.post.u_id == u_id) {
					var profile_pic = res.user.profile_pic == null ? 'profile.png' : res.user.profile_pic;
				} else {
					var profile_pic = res.post.u_image == null ? 'profile.png' : res.post.u_image;
				}
	 					
	 					var profile_link = "user_profile.php?id=" + res.post.u_id;
	 					var filesArray = res.post.files.replace("[","").replace("]","").split(',');
		 				posts_data = '<div class="posts_div" id="posts_div_'+ res.post.id +'"><div><span class="p_topics">Related Topics: '+ res.post.topics +'</span>'+
								'<span style="display:none;float: right;" id="edit_del_blk_'+ res.post.id +'">'+
									'<a href="edit_post.php?p_id='+ res.post.id +'&u_id='+ res.post.u_id +'">'+
										'<span class="glyphicon glyphicon-edit glp_edit"></span></a>&nbsp;'+
									'<span class="glyphicon glyphicon-trash glp_delete" onclick="onDelPost('+ res.post.id +')"></span></span></div>'+
						'<div class="posted_by col-md-12 m-sm-0 p-0" style="margin-bottom: 1%;margin-top: 1%;">'+
							'<div class="col-md-1 m-sm-0 p-0">'+
								'<a href="'+ profile_link +'"><img class="img-profile" src="./uploads/'+ profile_pic +'" width="50%"></a></div>'+
							'<div class="col-md-6 p-sm-0 p-0" style="margin-left: 8px"><a href="'+ profile_link +'"><span>'+ res.post.u_name +'</span></a><br><span class="posted_date">'+ moment(res.post.created_on).fromNow() +'</span></div></div>'+
						'<div style="margin-top: 1%;">'+
							'<a href="view_post.php?p_id='+ res.post.id +'" ><span class="post_title"><b>'+ res.post.p_title +'</b></span></a></div>'+
						'<div class="col-md-12 p-0">'+
							'<div class="col-md-12 p-0" id="post_desc_blk_'+ res.post.id +'">'+
								'<p style="font-size: 16px;">'+ res.post.p_desc +'</p></div></div>'+
						'<div class="col-md-12">'+
							'<div class="col-md-12" style="margin-bottom: 2%;" id="post_img_blk_'+ res.post.id +'">'+
							'</div></div>'+
						'<div style="margin-top: 1%;" class="rating"><div class="rat-left">'+
							'<span onclick="onLike('+ res.post.id +')">'+
								'<i class="fa home-fa-icons fa-thumbs-up " id="likes_'+ res.post.id +'"></i></span>'+
							'<span onclick="onDisLike('+ res.post.id +')">'+
								'<i class="fa home-fa-icons fa-thumbs-down " id="dis_likes_'+ res.post.id +'"></i></span>'+
							'<span onclick="onFav('+ res.post.id +')">'+
							'<i class="fa home-fa-icons fa-star " id="fav_'+ res.post.id +'"></i></span>'+
							'</div><div class="rat-right"><span>'+
							'<span onclick="onDownload('+ res.post.id +')">'+
								'<i class="fa home-fa-icons fa-download" ></i></span>'+
							'<span onclick="onShare('+ res.post.id +')">'+
								'<i class="fa home-fa-icons fa-share-alt" id="share_'+ res.post.id +'"></i></span>'+
							'<span onclick="clickCmntIcon('+ res.post.id +')">'+
								'<i class="fa home-fa-icons fa-comment"></i></span></span></div></div>'+
						'<div style="margin-top: 1%;"><span id="likes_tag_'+ res.post.id +'">'+ res.post.likes +'</span> Likes &nbsp;'+ 
							'<span id="dis_likes_tag_'+ res.post.id +'">'+ res.post.dis_likes +'</span> Dislikes'+
							'<span class="pull-right cmnt_tag"><span onclick="clickCmntIcon('+ res.post.id +')" id="cmnts_count_'+res.post.id +'">0 Comments</span></span></div>'+
							'<div id="jssocials_id_'+res.post.id +'" style="display:none;"></div>'+
							'<div class="cmnts_blk" style="display:none;"  id="cmnts_blk_'+res.post.id +'">'+
							'<div><ul class="cmnts_ul" id="cmnts_ul_'+res.post.id +'"></ul></div><div id="editor_blk_'+ res.post.id +'"><textarea name="editor" id="editor_'+res.post.id +'" rows="10" cols="80"></textarea>'+
							'<div style="margin-top:5px;"><button type="button" class="btn btn-default btn-sm" onclick="onComment('+res.post.id +')">ADD COMMENT</button></div></div></div></div>';


	 				$("#posts_div_blk").html(posts_data);


 					if (res.post.u_id == u_id) {
 						$("#edit_del_blk_"+ res.post.id).show();
 					}

 					if (filesArray.length > 0) {
				        var imgdata = '';
				        for(i=0; i< filesArray.length; i++)
				        {
				            var fileExt = filesArray[i].split('.').pop();
				            if (fileExt == 'mp3' || fileExt == 'wav') {
				                imgdata += '<div class="col-md-3 media_div"><audio controls style="width:100px">'+
	                                  '<source src="./uploads/'+ filesArray[i] +'" type="audio/mpeg">'+
	                            '</audio><a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge">Download</span></a></div>';
				            }
				            if (fileExt == 'mp4' || fileExt == '3gp') {
				                imgdata += '<div class="col-md-3 media_div"><video width="100" height="150" controls>'+
	                                  '<source src="./uploads/'+ filesArray[i] +'" type="video/mp4">'+
	                            '</video><a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge">Download</span></a></div>';'';
				            }
				            if (fileExt == 'pdf' || fileExt == 'doc' || fileExt == 'txt') {
				                imgdata += '<div class="col-md-3 media_div"><iframe width="100px" src="uploads/'+ filesArray[i] +'"></iframe><a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge">Download</span></a></div>';
				            }
				            if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif') {
				                imgdata += '<div class="col-md-3 media_div"><img src="uploads/'+ filesArray[i] +'" width="90%""><a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge">Download</span></a></div>';
				            }
				        }

				        $("#post_img_blk_"+res.post.id).html(imgdata)
				     }

 					CKEDITOR.replace( 'editor_'+res.post.id );

	 				for (var j = 0; j < res.like_status.length; j++) {
 						if (u_id == res.like_status[j].u_id && res.post.id == res.like_status[j].p_id) {
 							if (res.like_status[j].liked == 1) {
 								$("#likes_"+ res.post.id).addClass("like_active");
 							}
 						}
 					}

 					if (filesArray[0] == "") {
 						$("#post_img_blk_"+res.post.id).remove()
 						$("#post_desc_blk_"+res.post.id).css("width","100%");
 					}
 					cmnts_data = '';

 					for (var j = 0; j < res.cmnts.length; j++) {
 						if (res.post.id == res.cmnts[j].p_id) {
 							cmnts_data += "<li><div class='cmnt_div'><span class='cmnt_user'>"+ res.cmnts[j].u_name +"</span>&nbsp;&nbsp;<span style='opacity:0.8'>"+ res.cmnts[j].cmnt +"</span></div><div class='like_dislike_cmnt'><span id='like_cmnt_"+ res.cmnts[j].id +"' onclick='likeCmnt("+ res.cmnts[j].id +")'><i class='fa home-fa-icons fa-thumbs-up'></i></span>&nbsp;&nbsp;&nbsp;<span onclick='disLikeCmnt("+ res.cmnts[j].id +")' id='dislike_cmnt_"+ res.cmnts[j].id +"'><i class='fa home-fa-icons fa-thumbs-down'></i></span></div></li>"
 							$("#cmnts_ul_"+res.post.id).html(cmnts_data);
 							var li_length = $("#cmnts_ul_"+res.post.id+" li").length;
 							$("#cmnts_count_"+ res.post.id).text(+li_length+ " Comments");
 						}
 					}

 					for (var j = 0; j < res.cmnts.length; j++) {
 						if (u_id == res.cmnts[j].u_id) {
		 					if(res.cmnts[j].likes == 1) {
			 					$("#like_cmnt_"+ res.cmnts[j].id).addClass('like_dis_active');
			 				} else {
			 					$("#like_cmnt_"+ res.cmnts[j].id).removeClass('like_dis_active');
			 				}
			 				if(res.cmnts[j].dislikes == 1) {
			 					$("#dislike_cmnt_"+ res.cmnts[j].id).addClass('like_dis_active');
			 				} else {
			 					$("#dislike_cmnt_"+ res.cmnts[j].id).removeClass('like_dis_active');
			 				}
			 			}
		 			}
	 				

	 				for (var j = 0; j < res.dis_like_status.length; j++) {
 						if (u_id == res.dis_like_status[j].u_id && res.post.id == res.dis_like_status[j].p_id) {
 							if (res.dis_like_status[j].liked == 1) {
 								$("#dis_likes_"+ res.post.id).addClass("dis_like_active");
 							}
 						}
 					}
	 				

	 				for (var j = 0; j < res.fav_status.length; j++) {
 						if (u_id == res.fav_status[j].u_id && res.post.id == res.fav_status[j].p_id) {
 							if (res.fav_status[j].fav == 1) {
 								$("#fav_"+ res.post.id).addClass("fav_active");
 							}
 						}
 					}
	 				

					$("#jssocials_id_"+ res.post.id).jsSocials({
					    showLabel: false,
					    shareIn:"popup",
					    shares: [{
					        share: "facebook",
					        logo: "./assets/images/fb.png"
					    }, {
					        share: "twitter",
					        logo: "./assets/images/twitter.png"
					    }, {
					        share: "whatsapp",
					        logo: "./assets/images/watsapp.png"
					    }, {
					        share: "googleplus",
					        logo: "./assets/images/google.png"
					    }, {
					        share: "linkedin",
					        logo: "./assets/images/linkedin.png"
					    }]
					});
					
 			}
 		},
 		error:function(err) {
 			console.log('error',err)
 		}
 	})
}
function onShare(id) {
	if (localStorage.userObj) {
		$("#jssocials_id_"+id).toggle(500);
    	$("#share_"+id).toggleClass("share_active");
	} else {
		window.location.href = "signin.php";
	}
    
}

function onDownload(id) {
	if (localStorage.userObj) {
		var post = posts_arr.find(item => item.id == id);
    	var filesArray = post.files.replace("[","").replace("]","").split(',');

	     if (filesArray.length > 0) {
	        var imgdata = '';
	        for(i=0; i< filesArray.length; i++)
	        {
	            var fileExt = filesArray[i].split('.').pop();
	            if (fileExt == 'mp3' || fileExt == 'wav') {
	                imgdata += '<a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> Audio</span></a>';    
	            }
	            if (fileExt == 'mp4' || fileExt == '3gp') {
	                imgdata += '<a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> Video</span></a>';
	            }
	            if (fileExt == 'pdf' || fileExt == 'doc' || fileExt == 'txt') {
	                imgdata += '<a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> File</span></a>';
	            }
	            if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif') {
	                imgdata += '<a href="uploads/'+ filesArray[i] +'" download><span class="badge attch_badge"><span class="glyphicon glyphicon-paperclip"></span> Image</span></a>';
	            }
	        }
	        $('#pre_post_media').html(imgdata);
	        $('#pre_post_title').text(post.p_title);
	        $('#pre_post_desc').html(post.p_desc);
	        $('#pre_topics').text(post.topics);
	        $('#pdf_btn').html('<span class="badge attch_badge" onclick="downloadPost('+ id +')">Download Post</span>');
	        $("#preview_post_modal").modal();
	     } else {
	        $('#pre_post_media').html('No Attachments');
	        $('#pre_post_title').text(post.p_title);
	        $('#pre_post_desc').html(post.p_desc);
	        $('#pre_topics').text(post.topics);
	        $('#pdf_btn').html('<span class="badge attch_badge" onclick="downloadPost('+ id +')">Download Post</span>');
	        $("#preview_post_modal").modal();
	     }
	} else {
		window.location.href = "signin.php";
	}
    
}

function downloadPost(id) {
    var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    doc.fromHTML($('#pdf_div_'+id).html(), 15, 15, {
        'width': 170,
            'elementHandlers': specialElementHandlers
    });
    doc.save('post.pdf');
}

function onDelPost(post_id) {
	 if(confirm("Do you really want to delete this post?")) {
	 	$.ajax({
	 		url:'del_post_fun.php',
	 		type:'post',
	 		data:{post_id:post_id},
	 		dataType:'json',
	 		success:function(res) {
	 			console.log(res);
	 			if (res.success == true) {
	 				posts_data = '';
	 				getAllData();
	 			}
	 		},
	 		error:function(err) {
	 			console.log('error',err)
	 		}
	 	})
	 }
}

function onComment(post_id) {
	if (localStorage.userObj) {
		$("#editor_blk_"+post_id).hide(500);

		var id = 'editor_'+post_id;
		var user = JSON.parse(localStorage.getItem('userObj'));

		var content = CKEDITOR.instances[id].getData();
		var cmnt = content.replace(/<\/?[^>]+(>|$)/g, "");

		$.ajax({
	 		url:'add_cmnt_fun.php',
	 		type:'post',
	 		data:{u_id:user.id,u_name:user.fname, p_id:post_id,cmnt:cmnt},
	 		dataType:'json',
	 		success:function(res) {
	 			console.log(res);
	 			if (res.success == true) {
	 				var add_cmnts_data = '';
	 				if (res.cmnts.length > 0) {
	 					for (var j = 0; j < res.cmnts.length; j++) {
	 						add_cmnts_data += "<li><div class='cmnt_div'><span class='cmnt_user'>"+ res.cmnts[j].u_name +"</span>&nbsp;&nbsp;<span style='opacity:0.8'>"+ res.cmnts[j].cmnt +"</span></div><div class='like_dislike_cmnt'><span id='like_cmnt_"+ res.cmnts[j].id +"' onclick='likeCmnt("+ res.cmnts[j].id +")'><i class='fa home-fa-icons fa-thumbs-up'></i></span>&nbsp;&nbsp;&nbsp;<span onclick='disLikeCmnt("+ res.cmnts[j].id +")' id='dislike_cmnt_"+ res.cmnts[j].id +"'><i class='fa home-fa-icons fa-thumbs-down'></i></span></div></li>"

		 					$("#cmnts_ul_"+post_id).html(add_cmnts_data);
		 					var li_length = $("#cmnts_ul_"+post_id+" li").length;
		 					$("#cmnts_count_"+ post_id).text(+li_length+" Comments");
	 					}
	 				}
	 				
	 			}
	 		},
	 		error:function(err) {
	 			console.log('error',err)
	 		}
	 	})
	} else {
		window.location.href = "signin.php";
	}
	

}

function clickCmntIcon(post_id) {
	if (localStorage.userObj) {
    	$("#cmnts_blk_"+post_id).toggle(500);
    } else {
    	window.location.href = "signin.php";
    }
}

function likeCmnt(cmnt_id) {
    if (localStorage.userObj) {
        var tag = "Like";
        $.ajax({
            url:'like_dis_cmnt_fun.php',
            type:'post',
            data:{tag:tag, cmnt_id:cmnt_id},
            dataType:'json',
            success:function(res) {
                console.log(res);
                if (res.success == true) {
                    if(res.like_dis.likes == 1) {
                        $("#like_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#like_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                    if(res.like_dis.dislikes == 1) {
                        $("#dislike_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#dislike_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                }
            },
            error:function(err) {
                console.log('error',err)
            }
        })
    } else {
        window.location.href = 'signin.php';
    }
}

function disLikeCmnt(cmnt_id) {
    if (localStorage.userObj) {
        var tag = "Dislike";
        $.ajax({
            url:'like_dis_cmnt_fun.php',
            type:'post',
            data:{tag:tag, cmnt_id:cmnt_id},
            dataType:'json',
            success:function(res) {
                console.log(res);
                if (res.success == true) {
                    if(res.like_dis.likes == 1) {
                        $("#like_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#like_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                    if(res.like_dis.dislikes == 1) {
                        $("#dislike_cmnt_"+ cmnt_id +' i').addClass('like_dis_active');
                    } else {
                        $("#dislike_cmnt_"+ cmnt_id +' i').removeClass('like_dis_active');
                    }
                }
            },
            error:function(err) {
                console.log('error',err)
            }
        })
    } else {
        window.location.href = 'signin.php';
    }
}

function onLike(post_id) {
	if (localStorage.userObj) {
		var user = JSON.parse(localStorage.getItem('userObj'));

		$.ajax({
	 		url:'like_post_fun.php',
	 		type:'post',
	 		data:{u_id:user.id, post_id:post_id},
	 		dataType:'json',
	 		success:function(res) {
	 			console.log(res);
	 			if (res.success == true) {
	 				
	 				if (res.like_status.liked == '1') {
	 					$("#likes_"+ post_id).addClass('like_active');
	 				} else {
	 					$("#likes_"+ post_id).removeClass('like_active');
	 				}

	 				$('#likes_tag_'+ post_id).text(res.likes);

	 			}
	 		},
	 		error:function(err) {
	 			console.log('error',err)
	 		}
	 	})
	} else {
		window.location.href = "signin.php";
	}

	
}

function onDisLike(post_id) {
	if (localStorage.userObj) {
		var user = JSON.parse(localStorage.getItem('userObj'));

		$.ajax({
	 		url:'dislike_post_fun.php',
	 		type:'post',
	 		data:{u_id:user.id, post_id:post_id},
	 		dataType:'json',
	 		success:function(res) {
	 			console.log(res);
	 			if (res.success == true) {

	 				if (res.dis_like_status.liked == '1') {
	 					$("#dis_likes_"+ post_id).addClass('dis_like_active');
	 				} else {
	 					$("#dis_likes_"+ post_id).removeClass('dis_like_active');
	 				}

	 				$('#dis_likes_tag_'+ post_id).text(res.dis_likes);
	 			}
	 		},
	 		error:function(err) {
	 			console.log('error',err)
	 		}
	 	})
	} else {
		window.location.href = "signin.php";
	}
}

function onFav(post_id) {
	if (localStorage.userObj) {
		$("#fav_"+ post_id).toggleClass('fav_active');
		var user = JSON.parse(localStorage.getItem('userObj'));

		$.ajax({
	 		url:'fav_posts_fun.php',
	 		type:'post',
	 		data:{u_id:user.id, post_id:post_id},
	 		dataType:'json',
	 		success:function(res) {
	 			console.log(res);
	 			if (res.success == true) {
	 			}
	 		},
	 		error:function(err) {
	 			console.log('error',err)
	 		}
	 	})
 	} else {
		window.location.href = "signin.php";
	}
}