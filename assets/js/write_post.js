var topics_arr = [];
var imgs_arr = [];
var imgs_arr = [];
var ac_topics_arr = [];
var imgsdata = '';

$(document).ready(function(){
	CKEDITOR.replace( 'editor1' );

	getTopics();

	$('#add_topic').click(function() {
		var topic = $('#topic').val();
		if (topic != '') {
			if (topics_arr.indexOf(topic) == -1) {
				topics_arr.push(topic);
			}
		}

		$('#topic').val('');
		$('#topics').val(topics_arr);
	});

	$('#topics').change(function() {
		var topics_obj = $(this).val();
		var topicsArray = topics_obj.replace("[","").replace("]","").split(',');
		topics_arr = topicsArray;
		console.log(topics_arr)
	})

	$('#add_media').click(function() {
		$('#post_file').click();
	});

	$('#post_file').change(function() {
		$('#upload_btn').click();
	});

	$('#preview_post').click(function() {
		var p_title = $('#post_title').val();
		var p_desc = CKEDITOR.instances.editor1.getData();
		var cleanDesc = p_desc.replace(/<\/?[^>]+(>|$)/g, "");
		if (p_title.length < 10) {
			$('#p_title_err').text('Title should contain min 10 characters');
		} else {
			$('#p_title_err').text('');
		}
		if (cleanDesc.length < 20) {
			$('#p_desc_err').text('Description should contain min 20 characters');
		} else {
			$('#p_desc_err').text('');
		}

		var topics = $('#topics').val();
		if (imgs_arr.length > 0) {
			if (p_title.length >= 10 && cleanDesc.length >= 20) {				 
				$('#pre_post_title').text(p_title);
				$('#pre_post_desc').html(p_desc);
				$('#pre_topics').text(topics);
					$('#pre_post_media').html(imgsdata);
					$("#preview_post_modal").modal();
			}
		} else { 
			if (p_title.length >= 10 && cleanDesc.length >= 20) {
				$('#pre_post_title').text(p_title);
				$('#pre_post_desc').html(p_desc);
				$('#pre_topics').text(topics);
				$('#pre_post_media').html('');
				$("#preview_post_modal").modal();				
			}
		}
		return false;
	});

	$('#submit_post').click(function() {
		var p_title = $('#post_title').val();
		// var title_words = $.trim(p_title).split(/\s+/);
		var p_desc = CKEDITOR.instances.editor1.getData();
		var topics = $('#topics').val();
		var cleanDesc = p_desc.replace(/<\/?[^>]+(>|$)/g, "");
		// var desc_words = $.trim(cleanDesc).split(/\s+/);

		if (p_title.length < 10) {
			$('#p_title_err').text('Title should contain min 10 characters');
		} else {
			$('#p_title_err').text('');
		}
		if (cleanDesc.length < 20) {
			$('#p_desc_err').text('Description should contain min 20 characters');
		} else {
			$('#p_desc_err').text('');
		}

		if (p_title.length >= 10 && cleanDesc.length >= 20) {
			var user = JSON.parse(localStorage.getItem('userObj'));
			if (user.profile_pic == null) {
				var u_image = 'profile.png';
			} else {
				var u_image = user.profile_pic;
			}
			var data = {post_title:p_title, p_desc:p_desc, topics:topics, u_id:user.id,u_name:user.fname, u_image:u_image, images:imgs_arr};

			$.ajax({
				url:'save_post_fun.php',
				type:'post',
				data:data,
				dataType:'json',
				success:function(res) {
					console.log(res);
					if (res.success == true) {
						saveTopics();
						window.location.href = 'index.php?id='+ user.id +'';
					}
				},
				error:function(err) {
					console.log('error',err);
				}
			})
		}
		return false;
	});

	$("#reset_post").click(function() {
		CKEDITOR.instances.editor1.setData('');
		imgsdata = '';
		$("#media_blk").html('No Media');
		topics_arr = [];
	});

	$('#create_post_form').ajaxForm({
	    beforeSubmit: function() {
	      $('#progress_bar').css('display','inherit');
	      var percentComplete = 0;
	      var percentVal = percentComplete + '%';
	      $('.progress-bar').css('width',percentVal).text(percentVal);
	    },

	    uploadProgress: function(event, position, total, percentComplete) {
	      var percentVal = percentComplete + '%';
	      $('.progress-bar').css('width',percentVal).text(percentVal);
	    },
	    
		success: function() {
	      var percentVal = '100%';
	      $('.progress-bar').css('width',percentVal).text(percentVal);
	    },

	    complete: function(xhr) {
	    	res = JSON.parse(xhr.responseText);
	    	console.log('file upload',res);
	    	imgs = res.imgs;
	      	if (imgs.length > 0) {
				for(i=0; i<imgs.length; i++) {
					imgs_arr.push(imgs[i]);
					
					var fileExt = imgs[i].split('.').pop();
					if (fileExt == 'mp3' || fileExt == 'wav') {
	                   imgsdata += '<audio controls style="width:100px">'+
	                          '<source src="./uploads/'+ imgs[i] +'" type="audio/mpeg">'+
	                    '</audio>';

	                }
	                if (fileExt == 'mp4' || fileExt == '3gp') {
	                    imgsdata += '<video width="100" height="150" controls>'+
	                          '<source src="./uploads/'+ imgs[i] +'" type="video/mp4">'+
	                    '</video>';

	                }
	                if (fileExt == 'pdf' || fileExt == 'doc' || fileExt == 'txt') {
	                    imgsdata += '<iframe width="100px" src="uploads/'+ imgs[i] +'"></iframe>'; 
	                }
	                if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif' || fileExt == 'jpeg') {
						imgsdata += '<div class="col-md-3 media_div"><img src="uploads/'+ imgs[i] +'" width="90%""></div>';
					}
				}

				$("#media_blk").html(imgsdata);
				$('#progress_bar').css('display','none');
				$('.progress-bar').css('width','0%').text('0%');
	    	}
		} 
	});
});

function saveTopics() {
	var topics = $('#topics').val();

	$.ajax({
		url:'save_topics_fun.php',
		type:'post',
		data:{topics:topics},
		dataType:'json',
		success:function(res) {
			console.log(res);
		},
		error:function(err) {
			console.log('error',err);
		}
	})
	return false;
}

function getTopics() {
	$.ajax({
		url:'get_topics_fun.php',
		type:'GET',
		dataType:'json',
		success:function(res) {
			console.log(res);
			if (res.success == true) {
				for (var i = 0; i < res.topics.length; i++) {
					ac_topics_arr.push(res.topics[i].name);
				}
				$( "#topic" ).autocomplete({
			      source: ac_topics_arr
			    });
			}
		},
		error:function(err) {
			console.log('error',err);
		}
	})
}