var imgsdata = '';
var imgs_arr = [];
var del_imgs_arr = [];
var topics_arr = [];
var ac_topics_arr = [];

$(document).ready(function(){
	CKEDITOR.replace( 'editor1' );
	getPostData();
	getTopics();

	$('#add_media').click(function() {
		$('#post_file').click();
	});

	$('#add_topic').click(function() {
		var topic = $('#topic').val();
		topics_arr.push(topic);
		$('#topic').val('');
		$('#topics').val(topics_arr);
	});

	$('#post_file').change(function() {
		var ajaxTime= new Date().getTime();
		$('#progress_bar').css('display','inherit');
		for (var i = 1; i <= 100; i++) {
			var pro_bar_txt = i+'%';
			$('.progress-bar').css('width',pro_bar_txt).attr('aria-valuenow', i).text(pro_bar_txt);
		}
		var formdata = new FormData($('#create_post_form')[0]);
		for (var i = 0; i < $('#post_file').get(0).files.length; i++) {
		    formdata.append('postfiles[]', $('#post_file').get(0).files[i]);
		}

		$.ajax({
			url:'upload_img_fun.php',
			type:'post',
			data:formdata,
			dataType:'json',
			contentType: false,
			processData: false,
			success:function(res) {
				console.log(res);
				var totalTime = new Date().getTime()-ajaxTime;
				if (res.success == true) {
					if (res.imgs.length > 0) {
						for(i=0; i<res.imgs.length; i++) {
							imgs_arr.push(res.imgs[i]);
							var img_txt = `'${res.imgs[i]}'`;
							var fileExt = res.imgs[i].split('.').pop();
							if (fileExt == 'mp3' || fileExt == 'wav') {
	                           imgsdata += '<div class="col-md-3 media_div"><audio controls style="width:100px">'+
	                                  '<source src="./uploads/'+ res.imgs[i] +'" type="audio/mpeg">'+
	                            '</audio><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>';
	                             
	                        }
	                        if (fileExt == 'mp4' || fileExt == '3gp') {
	                            imgsdata += '<div class="col-md-3 media_div"><video width="100" height="150" controls>'+
	                                  '<source src="./uploads/'+ res.imgs[i] +'" type="video/mp4">'+
	                            '</video><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>';
	                             
	                        }
	                        if (fileExt == 'pdf' || fileExt == 'doc' || fileExt == 'txt') {
	                            imgsdata += '<div class="col-md-3 media_div"><iframe width="100px" src="uploads/'+ res.imgs[i] +'"></iframe><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>'; 
	                        }
	                        if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif' || fileExt == 'jpeg') {
								imgsdata += '<div class="col-md-3 media_div"><img src="uploads/'+ res.imgs[i] +'" width="90%""><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>';
							}
						}
						var pro_txt = $(".progress-bar").text();
						if (pro_txt === '100%') {
							setTimeout(function () {
							$('#progress_bar').css('display','none');
							$("#media_blk").html(imgsdata);
							}, totalTime);
						} 	
					}
				}
			},
			error:function(err) {
				console.log('error',err);
			}
		})
	})

	$('#preview_post').click(function() {
		var p_title = $('#post_title').val();
		var p_desc = CKEDITOR.instances.editor1.getData();
		var cleanDesc = p_desc.replace(/<\/?[^>]+(>|$)/g, "");
		if (p_title.length < 10) {
			$('#p_title_err').text('Title should contain min 10 characters');
		} else {
			$('#p_title_err').text('');
		}
		if (cleanDesc.length < 20) {
			$('#p_desc_err').text('Description should contain min 20 characters');
		} else {
			$('#p_desc_err').text('');
		}

		var topics = $('#topics').val();

		var formdata = new FormData($('#create_post_form')[0]);
		if (imgs_arr.length > 0) {
			if (p_title.length >= 10 && cleanDesc.length >= 20) {
				$('#pre_post_title').text(p_title);
				$('#pre_post_desc').html(p_desc);
				$('#pre_topics').text(topics);
            	imgdata = '';
            	for(i=0;i< imgs_arr.length;i++)
            	{
        			imgdata += "<img src='uploads/"+imgs_arr[i]+"' width='100px' height='100px' class='img-thumbnail'>";
        		}
				$('#pre_post_media').html(imgdata);
				$("#preview_post_modal").modal();
			}
		} else {
			if (p_title.length >= 10 && cleanDesc.length >= 20) {
				$('#pre_post_title').text(p_title);
				$('#pre_post_desc').html(p_desc);
				$('#pre_topics').text(topics);
				$('#pre_post_media').html('');
				$("#preview_post_modal").modal();
			}
		}
		return false;
	});

	$('#submit_post').click(function() {

		var p_title = $('#post_title').val();
		var topics = $('#topics').val();
		var p_desc = CKEDITOR.instances.editor1.getData();
		var cleanDesc = p_desc.replace(/<\/?[^>]+(>|$)/g, "");

		var post_id = $("#post_id").val();

		if (p_title.length < 10) {
			$('#p_title_err').text('Title should contain min 10 characters');
		} else {
			$('#p_title_err').text('');
		}
		if (cleanDesc.length < 20) {
			$('#p_desc_err').text('Description should contain min 20 characters');
		} else {
			$('#p_desc_err').text('');
		}

		if (p_title.length >= 10 && cleanDesc.length >= 20) {
			
			var user = JSON.parse(localStorage.getItem('userObj'));
			var data = {post_id:post_id,post_title:p_title,p_desc:p_desc,topics:topics,u_id:user.id,u_name:user.fname,images:imgs_arr,del_imgs:del_imgs_arr};
			$.ajax({
				url:'update_post_fun.php',
				type:'post',
				data:data,
				dataType:'json',
				success:function(res) {
					console.log(res);
					if (res.success == true) {
						saveTopics();
						window.location.href = 'index.php?id='+ user.id +'';
					}
				},
				error:function(err) {
					console.log('error',err);
				}
			})
		}
		return false;
	});
});

function getPostData() {
	var post_id = $("#post_id").val();
	$.ajax({
		url:'get_post_fun.php',
		type:'post',
		data:{post_id:post_id},
		dataType:'json',
		success:function(res) {
			console.log(res);
			if (res.success == true) {
				$("#post_title").val(res.post.p_title);
				CKEDITOR.instances.editor1.setData(res.post.p_desc);
				$("#topics").val(res.post.topics);
				var topicsArray = res.post.topics.replace("[","").replace("]","").split(',');
				topics_arr = topicsArray;
				imgs_arr = res.imgs;
				if (imgs_arr.length > 0) {
					for(i=0; i<imgs_arr.length; i++) {
						var fileExt = res.imgs[i].split('.').pop();
						var img_txt = `'${imgs_arr[i]}'`;
						if (fileExt == 'mp3' || fileExt == 'wav') {
                           imgsdata += '<div class="col-md-3 media_div"><audio controls style="width:100px">'+
                                  '<source src="./uploads/'+ res.imgs[i] +'" type="audio/mpeg">'+
                            '</audio><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>';
                             
                        }
                        if (fileExt == 'mp4' || fileExt == '3gp') {
                            imgsdata += '<div class="col-md-3 media_div"><video width="100" height="150" controls>'+
                                  '<source src="./uploads/'+ res.imgs[i] +'" type="video/mp4">'+
                            '</video><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>';
                             
                        }
                        if (fileExt == 'pdf' || fileExt == 'doc' || fileExt == 'txt') {
                            imgsdata += '<div class="col-md-3 media_div"><iframe width="100px" src="uploads/'+ res.imgs[i] +'"></iframe><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>'; 
                        }
                        if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif' || fileExt == 'jpeg') {
							imgsdata += '<div class="col-md-3 media_div"><img src="uploads/'+ res.imgs[i] +'" width="90%""><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>';
						}
					}
					$("#media_blk").html(imgsdata);
				} else {
					$("#media_blk").html('<p class="text-center">No Media</p>');
				}
			}
		},
		error:function(err) {
			console.log('error',err);
		}
	})
}

function delImgFun(file) {
	del_imgs_arr.push(file);
	var index = imgs_arr.indexOf(file);
	if (index > -1) {
	  imgs_arr.splice(index, 1);
	}

	if (imgs_arr.length > 0) {
		imgsdata = '';
		for(i=0; i<imgs_arr.length; i++) {
			var img_txt = `'${imgs_arr[i]}'`;
			imgsdata += '<div class="col-md-3 media_div"><img src="uploads/'+ imgs_arr[i] +'" width="90%""><span class="glyphicon glyphicon-remove del_icon" onclick="delImgFun('+img_txt+')"></span></div>';
		}
		$("#media_blk").html(imgsdata);
	} else {
		$("#media_blk").html('<p class="text-center">No Media</p>');
	}
	console.log(imgs_arr,del_imgs_arr);
}

function saveTopics() {
	var topics = $('#topics').val();

	$.ajax({
		url:'save_topics_fun.php',
		type:'post',
		data:{topics:topics},
		dataType:'json',
		success:function(res) {
			console.log(res);
		},
		error:function(err) {
			console.log('error',err);
		}
	})
	return false;
}

function getTopics() {
	$.ajax({
		url:'get_topics_fun.php',
		type:'GET',
		dataType:'json',
		success:function(res) {
			console.log(res);
			if (res.success == true) {
				for (var i = 0; i < res.topics.length; i++) {
					ac_topics_arr.push(res.topics[i].name);
				}
				$( "#topic" ).autocomplete({
			      source: ac_topics_arr
			    });
			}
		},
		error:function(err) {
			console.log('error',err);
		}
	})
}