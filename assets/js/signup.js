$(document).ready(function(){
 $('.join_btn').click(function() {
 	var data = $('.signup_form').serialize();

 	var fname = $('.f_name').val();
 	var lname = $('.l_name').val();
 	var email = $('.email').val();
 	var pass = $('.pass').val();
 	var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

 	if (fname == '') {
 		$('#form_err').removeClass('hide');
 		$('#form_err').text('First Name required');
 		return false;
 	}
 	if (lname == '') {
 		$('#form_err').removeClass('hide');
 		$('#form_err').text('Last Name required');
 		return false;
 	}
 	if (email == '') {
 		$('#form_err').removeClass('hide');
 		$('#form_err').text('Email required');
 		return false;
 	}
 	if (pass == '') {
 		$('#form_err').removeClass('hide');
 		$('#form_err').text('Password required');
 		return false;
 	}
 	if(!strongRegex.test(pass)) {
 		$('#form_err').removeClass('hide');
 		$('#form_err').text('Password should contain min 8 characters with atleast 1 special character, 1 number, uppercase and lowercase letters');
 		return false;
 	}

 	if (fname != '' && lname != '' && email != '' && pass != '' && strongRegex.test(pass)) {
 		$('#form_err').addClass('hide');
 		$('#form_err').text('');

 		$.ajax({
	 		url:'signup_fun.php',
	 		type:'post',
	 		data:data,
	 		dataType:'json',
	 		success:function(res) {
	 			console.log(res);
	 			if (res.success == true) {
	 				$('#email_verf_msg').removeClass('hide');
	 				$('.signup_form')[0].reset();
	 				localStorage.setItem('userObj', JSON.stringify(res.user));
	 			} else {
	 				$('#form_err').removeClass('hide');
 					$('#form_err').text(res.messages);
	 			}
	 		},
	 		error:function(err) {
	 			console.log('error',err)
	 		}
	 	})
 		return false;
 	}

 })  
});
