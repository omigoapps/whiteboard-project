var p_d ='';
var alphabets = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
				'S','T','U','V','W','X','Y','Z'];
$(document).ready(function(){
 
 	 $('#sortby_alpha').click(function(){
	   $('#sortby_date').removeClass('sort_active');
	   $('#sortby_alpha').addClass('sort_active');
	 });

	$('#sortby_date').click(function(){
	  $('#sortby_alpha').removeClass('sort_active');
	  $('#sortby_date').addClass('sort_active');
		$.ajax({
			url:'topics_fun.php',
			type:'post',
			data:{sort:'date'},
			dataType:'json',
			success:function(res) {
				console.log(res); 
				topics_data = '';
				if (res.topics.length > 0) {
					for (var i = 0; i < res.topics.length; i++) {
						var date = new Date(res.topics[i].created_on).toLocaleDateString();
							topics_data += "<li class='date_li' id='date_"+ res.topics[i].id +"'>"+ date + "</li><li class='topics_li'>"+ res.topics[i].name + "</li>";
					}

					$("#topics_ul").html(topics_data);

					var arr = [];
					for (var i = 0; i < res.topics.length; i++) {
								var li_id = $("#date_"+res.topics[i].id).attr('id');
								var li_txt = $("#date_"+res.topics[i].id).text();
								arr.push({id:li_id,txt:li_txt});
					}

					arr = arr.reverse();
					for (var i = 0; i < arr.length - 1; i++) {
						if (arr[i+1].txt == arr[i].txt) {
							$("#"+arr[i].id).remove();
						}
					}

				}
			},
			error:function(err) {
				console.log('error',err);
			}
	   });		  
	});


});

getTopics();
function getTopics()
{
	$.ajax({
		url:'topics_fun.php',
		type:'post',
		data:{sort:'alphabetical'},
		dataType:'json',
		success:function(res) {
			console.log(res);
			if (res.topics.length > 0) {
				var topics_data = '';

				for (var i = 0; i < res.topics.length; i++) {
					for (var j = 0; j < alphabets.length; j++) {
						if (res.topics[i].name.charAt(0).toLowerCase() == alphabets[j].charAt(0).toLowerCase()) {
							topics_data += "<li class='alphabets_li' id='alp_"+ res.topics[i].id +"'>"+ alphabets[j] + "</li><li class='topics_li'>"+ res.topics[i].name + "</li>";
							
						}
					}
				}

				$("#topics_ul").html(topics_data);
				var arr = [];
				for (var i = 0; i < res.topics.length; i++) {
					for (var j = 0; j < alphabets.length; j++) {
						if ($("#alp_"+res.topics[i].id).text().charAt(0).toLowerCase() == alphabets[j].charAt(0).toLowerCase()) {
							var li_id = $("#alp_"+res.topics[i].id).attr('id');
							var li_txt = $("#alp_"+res.topics[i].id).text();
							arr.push({id:li_id,txt:li_txt});
						}
					}
				}
				arr = arr.reverse();
				for (var i = 0; i < arr.length - 1; i++) {
					if (arr[i+1].txt == arr[i].txt) {
						$("#"+arr[i].id).remove();
					}
				}
			}
		},
		error:function(err) {
			console.log('error',err);
		}
   })
}

