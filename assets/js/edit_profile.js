$(document).ready(function(){
	var user = JSON.parse(localStorage.getItem('userObj'));
	$('#u_name').val(user.fname);
	$('#u_email').val(user.email);
	$('#mobile').val(user.mobile);
	$('#about').val(user.about);
	$('#city').val(user.city);
	$('#country').val(user.country);
	if (user.profile_pic != null) {
		$('#upload_pic').attr('src','./uploads/'+user.profile_pic+'');
	}
	
	$('#pic_change').click(function() {
		$('#file_input').click();
	})

	$('#cancel_edit_profile').click(function() {
		var user = JSON.parse(localStorage.getItem('userObj'));
		window.location.href = 'user_profile.php?id='+ user.id +'';
	})

	$('#file_input').change(function() {
		var user = JSON.parse(localStorage.getItem('userObj'));
		var formdata = new FormData($('#edit_profile_form')[0]);
		formdata.append('id', user.id);
		$.ajax({
			url:'save_image.php',
			type:'post',
			data:formdata,
			dataType:'json',
			contentType: false,
			processData: false,
			success:function(res) {
				console.log(res);
				if (res.success == true) {
					$('#upload_pic').attr('src','./uploads/'+res.user.profile_pic+'');
					localStorage.setItem('userObj', JSON.stringify(res.user));
				} else {
					$('#form_err').removeClass('hide');
					$('#form_err').text(res.messages);
				}
			},
			error:function(err) {
				console.log('error',err);
			}
		})
	});

	$('#submit_edit_profile').click(function() {
		var u_name = $('#u_name').val();
		var u_email = $('#u_email').val();
		var mobile = $('#mobile').val();
		var about = $('#about').val();
		var city = $('#city').val();
		var country = $('#country').val();
		var educations = $('#educations').val();

		// if (u_name == '') {
		// 	$('#form_err').removeClass('hide');
		// 	$('#form_err').text('Name required');
		// 	return false;
		// }
		// if (u_email == '') {
		// 	$('#form_err').removeClass('hide');
		// 	$('#form_err').text('Email required');
		// 	return false;
		// }
		// if (mobile == '') {
		// 	$('#form_err').removeClass('hide');
		// 	$('#form_err').text('Mobile Number required');
		// 	return false;
		// }
		// if (about == '') {
		// 	$('#form_err').removeClass('hide');
		// 	$('#form_err').text('About Us required');
		// 	return false;
		// }
		// if (city == '') {
		// 	$('#form_err').removeClass('hide');
		// 	$('#form_err').text('City required');
		// 	return false;
		// }
		// if (country == 'Select') {
		// 	$('#form_err').removeClass('hide');
		// 	$('#form_err').text('Country required');
		// 	return false;
		// }
		// if (educations == '') {
		// 	$('#form_err').removeClass('hide');
		// 	$('#form_err').text('Educations required');
		// 	return false;
		// }

		var user = JSON.parse(localStorage.getItem('userObj'));

		// if (u_name != '' && u_email != '' && mobile != '' && about != '' && city != '' && country != 'Select' && educations != '') {
			$.ajax({
				url:'edit_profile_fun.php',
				type:'post',
				data:{name:u_name,email:u_email,mobile:mobile,about:about,educations:educations,city:city,country:country,id:user.id},
				dataType:'json',
				success:function(res) {
					console.log(res);
					if (res.success == true) {
						localStorage.setItem('userObj', JSON.stringify(res.user));
						var user = JSON.parse(localStorage.getItem('userObj'));
						window.location.href = 'user_profile.php?id='+ user.id +'';
					} else {
						$('#form_err').removeClass('hide');
						$('#form_err').text(res.messages);
					}
				},
				error:function(err) {
					console.log('error',err);
				}
			})
		// }
		return false;
	})
});