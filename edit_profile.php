<?php session_start(); if (isset($_SESSION['user'])) { include('header.php');include('db.php'); ?>
    <div class="container m-sm-0">
        <div class="panel panel-default">
            <h3 class="text-center edit_prifle">Edit Profile</h3>
            <hr>
            <div class="panel-body m-sm-0" id="edit_profile_blk">
                <div class="alert alert-danger hide" id="form_err"> </div>
                <form name="edit_profile_form" id="edit_profile_form" method="POST" enctype="multipart/form-data">
                    <div class="col-md-12 m-sm-0">
                        <div class="col-md-8 m-sm-0">
                            <div class="col-md-8 m-sm-0">
                                <div class="col-md-3">
                                    <h4>Name</h4> </div>
                                <div class="col-md-5">
                                    <input type="text" name="name" id="u_name"> </div>
                            </div>
                            <div class="col-md-8 m-sm-0">
                                <div class="col-md-3">
                                    <h4>Email</h4> </div>
                                <div class="col-md-5">
                                    <input type="text" name="email" id="u_email"> </div>
                            </div>
                            <div class="col-md-8 m-sm-0">
                                <div class="col-md-3">
                                    <h4>Mobile</h4> </div>
                                <div class="col-md-5">
                                    <input type="text" name="mobile" id="mobile"> </div>
                            </div>
                            <div class="col-md-8 m-sm-0">
                                <div class="col-md-3">
                                    <h4>City</h4> </div>
                                <div class="col-md-5">
                                    <input type="text" name="city" id="city"> </div>
                            </div>
                            <div class="col-md-8 m-sm-0">
                                <div class="col-md-3">
                                    <h4>Country</h4> </div>
                                <div class="col-md-5">
                                    <select class="form-control" name="country" id="country">
                                        <option>Select</option>
                                        <option>Australia</option>
                                        <option>America</option>
                                        <option>Canada</option>
                                        <option>Russia</option>
                                        <option>Japan</option>
                                        <option>Germany</option>
                                        <option>India</option>
                                        <option>England</option>
                                        <option>Pakistan</option>
                                        <option>Nepal</option>
                                        <option>Europe</option>
                                        <option>Sri Lanka</option>
                                        <option>China</option>
                                        <option>Singapore</option>
                                        <option>Malasia</option>
                                        <option>Swizerland</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 m-sm-0">
                                <div class="col-md-2">
                                    <h4>About Us</h4> </div>
                                <div class="col-md-10">
                                    <textarea name="about" id="about" rows="5"> </textarea>
                                </div>
                            </div>
                            <div class="col-md-12 m-sm-0">
                                <div class="col-md-2">
                                    <h4>Educations</h4> </div>
                                <div class="col-md-10">
                                    <textarea name="educations" id="educations" rows="5"> </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"> <img src="./assets/images/profile.png" width="40%" id="upload_pic">
                            <br>
                            <button type="button" class="btn btn-default btn-xs" id="pic_change" style="margin:1% 0% 0% 11%;padding: 5px 15px;">Change</button>
                            <input type="file" name="profile_pic" id="file_input"> </div>
                    </div>
                    <div class="col-md-12 text-center edit-profile-button">
                        <hr>
                        <button id="submit_edit_profile" type="submit" class="btn btn-default">Save</button>
                        <button class="btn btn-default" type="button" id="cancel_edit_profile">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/check_user_session.js"></script>
        <script type="text/javascript" src="./assets/js/edit_profile.js"></script>
        <?php } else { header("Location: signin.php"); } ?>