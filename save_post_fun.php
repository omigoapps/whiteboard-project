<?php 

include('db.php');

$validator = array('success'=> false, 'messages'=> array());

// print_r($_POST);exit;
// print_r($_FILES);exit;
$p_title = $_POST['post_title'];
$p_desc = $_POST['p_desc'];
$topics = $_POST['topics'];
$u_id = $_POST['u_id'];
$u_name = $_POST['u_name'];
$u_image = $_POST['u_image'];
$images = $_POST['images'];

$file_names = implode(',', $images);

$check_post = mysqli_query($db,"SELECT * FROM posts_tbl");

if (!$check_post) {

	$create_posts_tbl = "CREATE TABLE posts_tbl (
		id INT AUTO_INCREMENT,
		p_title VARCHAR(255),
	    p_desc VARCHAR(255),
	    topics VARCHAR(255),
	    u_id VARCHAR(255),
	    u_name VARCHAR(255),
	    u_image VARCHAR(255),
	    files VARCHAR(255),
	    likes INT DEFAULT 0,
	    dis_likes INT DEFAULT 0,
	    downloads INT DEFAULT 0,
	    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	    updated_on DATETIME,
	    PRIMARY KEY (id)
	  )";

	$create_query = mysqli_query($db, $create_posts_tbl);
	$insert_post = mysqli_query($db, 'INSERT INTO posts_tbl (p_title, p_desc, topics, u_id, u_name, u_image, files) VALUES ("'.$p_title.'", "'.$p_desc.'", "'.$topics.'", "'.$u_id.'", "'.$u_name.'", "'.$u_image.'", "'.$file_names.'")');

	$validator['success'] = true;
	$validator['messages'] = "Post created successfully";

} else {
	$insert_post = mysqli_query($db, 'INSERT INTO posts_tbl (p_title, p_desc, topics, u_id, u_name, u_image, files) VALUES ("'.$p_title.'", "'.$p_desc.'", "'.$topics.'", "'.$u_id.'", "'.$u_name.'", "'.$u_image.'", "'.$file_names.'")');
	
	$validator['success'] = true;
	$validator['messages'] = "Post created successfully";

}

echo json_encode($validator);


?>