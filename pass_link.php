<?php include('header1.php') ?>
    <div class="container m-sm-0">
        <div class="signin_panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="text-center">Forgot Password</h3>
                    <hr>
                    <div class="col-md-12" style="margin-top: 1%;">
                        <div class="alert alert-danger hide text-center" id="form_err"> </div>
                        <div class="col-md-6 fpass-text"> <span style="float: right;margin-top: 1%;">Password recovery email: </span> </div>
                        <div class="col-md-6 fpass-field">
                            <input type="text" name="rec_email" id="rec_email" placeholder="Recovery Email" style="padding: 5px;"> </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <hr>
                        <button id="reset_pass" type="button" class="btn btn-default">Reset Password</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/check_user_session.js"></script>
        <script type="text/javascript" src="./assets/js/pass_link.js"></script>