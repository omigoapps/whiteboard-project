<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
    <title>Whiteboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="icon" href="./assets/images/logo.png" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="./assets/css/jssocials.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/jssocials-theme-flat.css">
    <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css">
    <link rel="stylesheet" href="https://qsf.ec.quoracdn.net/-3-fonts.q_serif.q_serif_regular.woff2-26-67835a3ba7110796.woff2">
    <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
</head>

<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid nav-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <!--  Search no mobile  -->
            <div class="nav-wrapper">
                <div class="search-toggle show-m">
                    <button class="menu_toggle"><i class="fa fa-search fa-fw fa-flip-horizontal"></i></button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="index.php"><img src="./assets/images/logo.png" id="site_logo" width="60px" height="60px"></a>
                </div>
            </div>
            <div class="search-wrapper">
                <input type="search" placeholder="Search" class="search-m"> </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="nav-right">
                    <div></div>
                    <ul class="nav navbar-nav navbar-right nav_elems3 nav-main">
                        <li><a href="index.php" class="main_tabs" id="posts_tab">Posts</a></li>
                        <li><a href="topics.php" class="main_tabs" id="topics_tab">Topics</a></li>
                        <li><a href="links.php" class="main_tabs" id="links_tab">Links</a></li>
                        <li><a href="favorite.php" class="main_tabs" id="fav_tab">Favorite</a></li>
                        <li><a href="#" class="main_tabs" id="chats_tab">Chats</a></li>
                        <li class="hide-m">
                            <a href="#">
                                <input type="search" placeholder="Search"> </a>
                        </li>
                    </ul>
                    <?php if (isset($_SESSION['user'])) { ?>
                        <ul class="nav navbar-nav navbar-right nav_elems3">
                            <li><a href="write_post.php?id=<?= $_SESSION['user']; ?>" id="write_post" class="nav2_tabs">Write Post</a></li>
                            <li>
                                <a class="nav2_tabs">
                                    <div class="dropdown" id="user_dd"> <a class="dropdown-toggle" data-toggle="dropdown" id="username">loading...</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" id="my_profile">My Profile</a></li>
                                            <li><a href="#">Help</a></li>
                                            <li><a href="#" id="signout" name="signout">Sign Out</a></li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <?php } else { ?>
                            <ul class="nav navbar-nav navbar-right nav_elems2">
                                <li><a href="signup.php" class="nav2_tabs">Sign Up</a></li>
                                <li><a href="signin.php" class="nav2_tabs">Login</a></li>
                            </ul>
                            <?php }
     ?>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<!--    <?php include('footer.php'); ?>-->
    <script>
        $(document).ready(function () {
            var url = window.location.pathname;
            if (url.indexOf('user_profile') >= 0 || url.indexOf('edit_profile') >= 0) {
                $('#posts_tab').removeClass('tab_active');
                $('#topics_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#username').css('border-bottom', '2px solid #fff');
            }
            if (url.indexOf('write_post') >= 0) {
                $('#posts_tab').removeClass('tab_active');
                $('#topics_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#write_post').addClass('tab_active');
            }
            if (url.indexOf('topics') >= 0) {
                $('#topics_tab').addClass('tab_active');
                $('#fav_tab').removeClass('tab_active');
                $('#posts_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#write_post').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
            }

            if (url.indexOf('index') >= 0) {
                $('#posts_tab').addClass('tab_active');

                $('#topics_tab').removeClass('tab_active');
                $('#fav_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#write_post').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
            }

            if (url.indexOf('links') >= 0) {
                $('#links_tab').addClass('tab_active');

                $('#topics_tab').removeClass('tab_active');
                $('#fav_tab').removeClass('tab_active');
                $('#posts_tab').removeClass('tab_active');
                $('#write_post').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
            }
            if (url.indexOf('favorite') >= 0) {
                $('#fav_tab').addClass('tab_active');
                $('#topics_tab').removeClass('tab_active');
                $('#posts_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#write_post').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
            }
            $('#user_dd').click(function () {
                $(this).toggleClass('open');
            })
            $('#posts_tab').click(function () {
                var user = JSON.parse(localStorage.getItem('userObj'));
                $('#topics_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#fav_tab').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
                if (!$(this).hasClass('tab_active')) {
                    $(this).addClass('tab_active');
                }
                window.location.href = 'index.php?id=' + user.id + '';
            })
            $('#topics_tab').click(function () {
                var user = JSON.parse(localStorage.getItem('userObj'));
                window.location.href = 'topics.php?id=' + user.id + '';
                $('#topics_tab').addClass('tab_active');
                $('#fav_tab').removeClass('tab_active');
                $('#posts_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#write_post').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
            })
            $('#links_tab').click(function () {
                $('#posts_tab').removeClass('tab_active');
                $('#topics_tab').removeClass('tab_active');
                $('#fav_tab').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
                if (!$(this).hasClass('tab_active')) {
                    $(this).addClass('tab_active');
                }
            })
            $('#fav_tab').click(function () {
                $('#posts_tab').removeClass('tab_active');
                $('#topics_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                $('#chats_tab').removeClass('tab_active');
                if (!$(this).hasClass('tab_active')) {
                    $(this).addClass('tab_active');
                }
                window.location.href = 'favorite.php';
            })
            $('#chats_tab').click(function () {
                $('#posts_tab').removeClass('tab_active');
                $('#topics_tab').removeClass('tab_active');
                $('#fav_tab').removeClass('tab_active');
                $('#links_tab').removeClass('tab_active');
                if (!$(this).hasClass('tab_active')) {
                    $(this).addClass('tab_active');
                }
            })

            $(".menu_toggle").click(function () {
                $(".search-wrapper").slideToggle();
            })
        });
    </script>