<?php session_start(); if(!isset($_SESSION['user'])) { include('header1.php'); ?>
    <div class="panel panel-default signin_panel">
        <div class="signin_blk"> <img src="./assets/images/logo.png" width="45" height="45">
            <h3>Sign In</h3>
            <div class="alert alert-danger hide" id="form_err"> </div>
            <form name="signin_form" class="signin_form">
                <div>
                    <input type="text" name="email" class="email" placeholder="Email or Phone"> </div>
                <div>
                    <input type="password" name="pass" class="pass" placeholder="Password"> </div>
                <br>
                <div>
                    <button class=" btn btn-default signin_btn">Sign In</button>
                </div>
                <br> </form>
            <div>
                <p><a href="pass_link.php">Forgot Password?</a></p>
            </div>
            <div>
                <p>Not on whiteboard? <a href="signup.php">Sign Up</a></p>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/signin.js"></script>
        <?php } else { header("Location: index.php"); } ?>