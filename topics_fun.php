<?php
include('db.php');

$validator = array('success'=> false, 'messages'=> array(),'topics'=> array());

$sort_type = $_POST['sort'];

if(!empty($sort_type))
{
	if($sort_type == 'alphabetical')
	{
		$topics = mysqli_query($db,"SELECT * FROM topics_tbl ORDER BY name ASC ");
		   
		 if($topics)
		 {
			  while($row = mysqli_fetch_assoc($topics)) {
			  	 array_push($validator['topics'], $row); 
			     $validator['success'] = true;    
			     $validator['messages'] = "Topics fetched by alphabetically";    
			  }	
		 }
		 else
		 {
		 	 $validator['success'] = false;    
			 $validator['messages'] = "Error in fetching";
		 }
	}
	else
	{
		 $topics = mysqli_query($db,"SELECT * FROM topics_tbl ORDER BY created_on ASC ");
		 if($topics)
		 {
			  while($row = mysqli_fetch_assoc($topics)) {
			     array_push($validator['topics'], $row);    
			     $validator['success'] = true;    
			     $validator['messages'] = "Topics fetched by date";
			  }	
		 }
		 else
		 {
		 	 $validator['success'] = false;    
			 $validator['messages'] = "Error in fetching";
		 }	
	}
}
else
{
	$validator['success'] = false;    
	$validator['messages'] = "Error in fetching";
  
}

echo json_encode($validator);

?>