<?php include('header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-sm-0">
                <div class="content-body">
                    <div class="col-md-3" id="posts_left_block">
                        <?php include('left_blk.php'); ?>
                    </div>
                    <div class="col-md-6 topic-body m-sm-0" style="width: 47%;">
                        <p style="font-size: 25px;border-bottom: 1px solid #e2e2e2;padding-bottom: 5px; padding-left: 15px;padding-right: 15px;"><b>Links</b></p>
                        <div style="float: right;margin-top: -10%;margin-right: 3%;" class="add-link-button text-left" data-toggle="modal" data-target="#add_link_modal">
                            <button class="btn btn-default">Add Link</button>
                        </div> 
                        <div id="links_div_blk"> </div>
                    </div>
                    <div class="col-md-3" id="posts_right_block">
                        right block....
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_link_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Link</h4> </div>
                <form action="links_process.php" method="post">
                    <div class="modal-body" id="download_blk">
                        <input type="text" name="url" placeholder="Post URL" class="form-control"> </div>
                    <div class="modal-footer">
                        <input type="submit" name="submit" class="btn btn-default" id="add_link" value="Add">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update_link_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Link</h4> </div>
                <form action="link_update.php" method="post">
                    <div class="modal-body" id="download_blk">
                        <input type="hidden" name="id" class="populate-id">
                        <input type="text" name="url" placeholder="Post URL" class="form-control populate-url"> </div>
                    <div class="modal-footer">
                        <input type="submit" name="submit" class="btn btn-default" id="update_link" value="Update">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
        <script type="text/javascript" src="./assets/js/check_user_session.js"></script>
        <!--        <script type="text/javascript" src="./assets/js/like_dislike.js"></script>-->
        <script type="text/javascript" src="./assets/js/moment.js"></script>
        <script type="text/javascript" src="./assets/js/jssocials.min.js"></script>
        <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
        <script>
            $(document).on('click', '.edit-btn', function () {
                $('#update_link_modal').find('.populate-id').val($(this).attr('data-id'))
                $('#update_link_modal').find('.populate-url').val($(this).attr('data-url'))
            });
        </script>
        <script>
            var links_data = '';
            var links_arr = [];
            $(document).ready(function () {
                getAllData();
            });

            function getAllData() {
                var user = JSON.parse(localStorage.getItem('userObj'));
                $("#links_div_blk").html('<p>Loading....</p>');
                $.ajax({
                    url: 'get_links_data.php'
                    , type: 'post'
                    , data: {
                        u_id: user.id
                    }
                    , dataType: 'json'
                    , success: function (res) {
                        console.log(res);
                        if (res.success == true) {
                            links_arr = res.links;
                            if (res.links.length > 0) {
                                for (var i = 0; i < res.links.length; i++) {
                                    var profile_link = "user_profile.php?id=" + res.links[i].u_id;
                                    var profile_pic = res.links[i][0].profile_pic;
                                    links_data += `<div class="link-container"><div class="links_div" id="links_div_${res.links[i].id}">
                                    <div class="posted_by col-md-12 m-sm-0 p-0"><div class="col-md-1 m-sm-0 p-sm-0 p-0" style="margin-bottom: 12px;">
                                        <a href="${profile_link}"><img src="./uploads/${profile_pic}" class="img-profile"></a></div><div class="col-md-6 m-sm-0 p-sm-0 p-0" style="margin-left: 10px"><a href="${profile_link}"><span>${res.links[i][0].fname}</span></a><br><span class="posted_date">${ moment(res.links[i].created_on).fromNow() }</span></div></div><div class="clearfix"></div></div>
                                   <div class="row"><div class="col-md-8">
                                    <div id="posts_div_2">
                                        <p class="title"> ${res.links[i].title}</p> 
                                    <p class= "link_url" > <a href = "${res.links[i].url}" >${res.links[i].url}</a></p></div></div><div class="col-md-4"><img src="${res.links[i].image}" width="100px"></div></div>
                                    `;
                                    if (user.id == res.links[i][0].id) {
                                        links_data += ` <div class="link_act_btns"><a class="edit-btn" data-toggle="modal" data-target="#update_link_modal" data-url="${res.links[i].url}" data-id="${res.links[i].id}"><span class="glyphicon glyphicon-edit glp_edit"></span></a><a href="link_delete.php/?id=${res.links[i].id}" class=""><span class="glyphicon glyphicon-trash glp_delete"></span></a></div>`;
                                    }
                                    links_data += `</div></div>`;
                                }
                                $("#links_div_blk").html(links_data);
                            }
                            else {
                                $("#links_div_blk").html('<p>No Links Avaliable</p>');
                            }
                        }
                    }
                    , error: function (err) {
                        console.log('error', err)
                    }
                })
            }

            function onDelPost(post_id) {
                if (confirm("Do you really want to delete this post?")) {
                    $.ajax({
                        url: 'link_delete.php'
                        , type: 'post'
                        , data: {
                            post_id: post_id
                        }
                        , dataType: 'json'
                        , success: function (res) {
                            console.log(res);
                            if (res.success == true) {
                                posts_data = '';
                                getAllData();
                            }
                        }
                        , error: function (err) {
                            console.log('error', err)
                        }
                    })
                }
            }
        </script>