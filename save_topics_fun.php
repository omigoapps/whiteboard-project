<?php 
include('db.php');
// print_r($_POST);

$validator = array('success'=> false, 'messages'=> array(),'topics'=> array());

$topics_arr = array();

$topics_arr = explode(',', $_POST['topics']);

$check_topics_tbl = mysqli_query($db, "SELECT * FROM topics_tbl");

if (!$check_topics_tbl) {

	$create_topics_tbl = "CREATE TABLE topics_tbl (
		id INT AUTO_INCREMENT,
		name VARCHAR(255),
		created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	    updated_on DATETIME,
	    PRIMARY KEY (id)
	)";

	$create_query = mysqli_query($db, $create_topics_tbl);

	foreach ($topics_arr as $topic) {
		$check_topic = mysqli_query($db, "SELECT * FROM topics_tbl WHERE name = '".$topic."'");
		if (mysqli_num_rows($check_topic) == 0) {
			$insert_topic = mysqli_query($db,"INSERT INTO topics_tbl (name) VALUES ('".$topic."')");
			
			$validator['success'] = true;
			$validator['messages'] = "Topics saved Successfully";
		} else {
			$validator['success'] = false;
			$validator['messages'] = "Topic existed";
		}

 		
	}

	$get_topics = mysqli_query($db,"SELECT * FROM topics_tbl ORDER BY created_on DESC");

	while ($row = mysqli_fetch_assoc($get_topics)) {
		$validator['topics'] = $row['name'];
	}

} else {
	foreach ($topics_arr as $topic) {
 		$check_topic = mysqli_query($db, "SELECT * FROM topics_tbl WHERE name = '".$topic."'");
		if (mysqli_num_rows($check_topic) == 0) {
			$insert_topic = mysqli_query($db,"INSERT INTO topics_tbl (name) VALUES ('".$topic."')");
			
			$validator['success'] = true;
			$validator['messages'] = "Topics saved Successfully";
		} else {
			$validator['success'] = false;
			$validator['messages'] = "Topic existed";
		}
	}

	$get_topics = mysqli_query($db,"SELECT * FROM topics_tbl ORDER BY created_on DESC");

	while ($row = mysqli_fetch_assoc($get_topics)) {
		$validator['topics'] = $row['name'];
	}

}


echo json_encode($validator);

?>