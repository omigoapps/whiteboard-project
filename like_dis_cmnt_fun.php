<?php 

include('db.php');

$validator = array('success'=> false, 'messages'=> array(), 'like_dis'=> array());

if ($_POST['tag'] == 'Like') {
	$get_like_status = mysqli_query($db, "SELECT likes FROM cmnts_tbl WHERE id = '".$_POST['cmnt_id']."'");

	$get_status = mysqli_fetch_assoc($get_like_status);

	if ($get_status['likes'] == 0) {
		$update_like_status = mysqli_query($db, "UPDATE cmnts_tbl SET likes = 1 WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis_q = mysqli_query($db, "SELECT * FROM cmnts_tbl WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis = mysqli_fetch_assoc($get_like_dis_q);

		$validator['success'] = true;
		$validator['messages'] = "Liked";
		$validator['like_dis'] = $get_like_dis;

	} else {
		$update_like_status = mysqli_query($db, "UPDATE cmnts_tbl SET likes = 0 WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis_q = mysqli_query($db, "SELECT * FROM cmnts_tbl WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis = mysqli_fetch_assoc($get_like_dis_q);

		$validator['success'] = true;
		$validator['messages'] = "Cancel Like";
		$validator['like_dis'] = $get_like_dis;
	}
	

}

if ($_POST['tag'] == 'Dislike') {
	$get_dislike_status = mysqli_query($db, "SELECT dislikes FROM cmnts_tbl WHERE id = '".$_POST['cmnt_id']."'");

	$get_status = mysqli_fetch_assoc($get_dislike_status);

	if ($get_status['dislikes'] == 0) {
		$update_dislike_status = mysqli_query($db, "UPDATE cmnts_tbl SET dislikes = 1 WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis_q = mysqli_query($db, "SELECT * FROM cmnts_tbl WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis = mysqli_fetch_assoc($get_like_dis_q);

		$validator['success'] = true;
		$validator['messages'] = "Disliked";
		$validator['like_dis'] = $get_like_dis;

	} else {
		$update_dislike_status = mysqli_query($db, "UPDATE cmnts_tbl SET dislikes = 0 WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis_q = mysqli_query($db, "SELECT * FROM cmnts_tbl WHERE id = '".$_POST['cmnt_id']."'");

		$get_like_dis = mysqli_fetch_assoc($get_like_dis_q);

		$validator['success'] = true;
		$validator['messages'] = "Cancel Dislike";
		$validator['like_dis'] = $get_like_dis;
	}
}

echo json_encode($validator);

?>