<?php

$media = array();

$validator = array('success'=> false, 'messages'=> array(), 'media'=> array()); 

if (isset($_FILES['postfiles'])) {

	for ($i=0; $i <count($_FILES['postfiles']['name']) ; $i++) { 
		$filetmp = $_FILES['postfiles']['tmp_name'][$i];
		$filename = $_FILES['postfiles']['name'][$i];
		$fileExt = explode('.', $filename);
		$fileActualExt = strtolower(end($fileExt));
		$file_name = uniqid().".".$fileActualExt;
		$filepath = "uploads/".$file_name;
		$moved = move_uploaded_file($filetmp, $filepath);
		if ($moved) {
			array_push($validator['media'], $file_name);
			$validator['success'] = true;
			$validator['messages'] = "Preview Media";
		} else {
			$validator['success'] = false;
			$validator['messages'] = "Upload error";
		}
	}

	echo json_encode($validator);
}

?>