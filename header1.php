<!DOCTYPE html>
<html>

<head>
    <title>Whiteboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="./assets/images/logo.png" type="image/x-icon" /> </head>

<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-container navbar-login">
                <div class="navbar-header">
                    <a href="index.php"><img src="./assets/images/logo.png" id="site_logo" width="60px" height="60px"></a>
                </div>
                <div class="nav-right">
                    <ul class="nav navbar-nav navbar-right nav_elems2">
                        <li><a href="signup.php" class="nav2_tabs">Sign Up</a></li>
                        <li><a href="signin.php" class="nav2_tabs">Login</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right nav_elems3 hide">
                        <li>
                            <a class="nav2_tabs">
                                <button class="btn btn-default">Write Post</button>
                            </a>
                        </li>
                        <li>
                            <a class="nav2_tabs">
                                <div class="dropdown" id="user_dd">
                                    <a class="dropdown-toggle" data-toggle="dropdown" id="username"></button>
                                        <ul class="dropdown-menu">
                                            <li> <a href="#">My Profile</a></li>
                                            <li><a href="#">Help</a></li>
                                            <li><a href="#" id="signout">Sign Out</a></li>
                                        </ul>
                                </div>
                                </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <?php include('footer.php'); ?>
    <script>
        $(document).ready(function () {
            $('#user_dd').click(function () {
                $(this).toggleClass('open');
            })
        });
    </script>