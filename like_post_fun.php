<?php 

include('db.php');

$validator = array('success'=> false, 'messages'=> array(), 'like_status'=> array(), 'likes'=> '');

$u_id = $_POST['u_id'];
$post_id = $_POST['post_id'];

$chk_liked_tbl = mysqli_query($db, "SELECT * FROM liked_users");

if (!$chk_liked_tbl) {
	$create_liked_tbl = "CREATE TABLE liked_users (
		id INT AUTO_INCREMENT,
		u_id VARCHAR(255),
		p_id VARCHAR(255),
		liked VARCHAR(255) DEFAULT 1,
		created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	    updated_on DATETIME,
	    PRIMARY KEY (id)
	)";

	$create_query = mysqli_query($db, $create_liked_tbl);

	$chk_user = mysqli_query($db, "SELECT liked FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

	if (mysqli_num_rows($chk_user) > 0) {
		$chk_like_status = mysqli_fetch_assoc($chk_user);
		if ($chk_like_status['liked'] == 1) {
			$dis_like = mysqli_query($db, "UPDATE liked_users SET liked = 0 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");
			$get_pre_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_likes_s = mysqli_fetch_assoc($get_pre_likes);

			$pre_likes = (int)$pre_likes_s['likes'];

			$pre_likes = $pre_likes -1;

			$update_likes = mysqli_query($db,"UPDATE posts_tbl SET likes = '".$pre_likes."' WHERE id = '".$post_id."'");

			$get_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_like_status = mysqli_query($db, "SELECT * FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$like_status = mysqli_fetch_assoc($get_like_status);

			$likes = mysqli_fetch_assoc($get_likes);

			$validator['likes'] = $likes['likes'];
			$validator['like_status'] = $like_status;
			$validator['success'] = true;
			$validator['messages'] = "Not Liked";
		} else {
			$dis_like = mysqli_query($db, "UPDATE liked_users SET liked = 1 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$get_pre_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_likes_s = mysqli_fetch_assoc($get_pre_likes);

			$pre_likes = (int)$pre_likes_s['likes'];

			$pre_likes = $pre_likes +1;

			$update_likes = mysqli_query($db,"UPDATE posts_tbl SET likes = '".$pre_likes."' WHERE id = '".$post_id."'");

			$get_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_like_status = mysqli_query($db, "SELECT * FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$like_status = mysqli_fetch_assoc($get_like_status);

			$likes = mysqli_fetch_assoc($get_likes);

			$validator['likes'] = $likes['likes'];
			$validator['like_status'] = $like_status;
			$validator['success'] = true;
			$validator['messages'] = "Liked";
		}

	} else {

		$insert_liked = mysqli_query($db,"INSERT INTO liked_users (u_id, p_id) VALUES ('".$u_id."','".$post_id."')");

		$get_pre_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = ".$post_id."");

		$pre_likes_s = mysqli_fetch_assoc($get_pre_likes);

		$pre_likes = (int)$pre_likes_s['likes'];

		$pre_likes = $pre_likes +1;

		$update_likes = mysqli_query($db,"UPDATE posts_tbl SET likes = '".$pre_likes."' WHERE id = '".$post_id."'");

		$get_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = '".$post_id."'");

		$get_like_status = mysqli_query($db, "SELECT * FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

		$like_status = mysqli_fetch_assoc($get_like_status);

		$likes = mysqli_fetch_assoc($get_likes);

		$validator['success'] = true;
		$validator['like_status'] = $like_status;
		$validator['likes'] = $likes['likes'];
		$validator['messages'] = "Liked";
	}
	
}


 else {

	$chk_user = mysqli_query($db, "SELECT * FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

	if (mysqli_num_rows($chk_user) > 0) {
		$chk_like_status = mysqli_fetch_assoc($chk_user);
		if ($chk_like_status['liked'] == 1) {
			$dis_like = mysqli_query($db, "UPDATE liked_users SET liked = 0 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");
			$get_pre_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_likes_s = mysqli_fetch_assoc($get_pre_likes);

			$pre_likes = (int)$pre_likes_s['likes'];

			$pre_likes = $pre_likes -1;

			$update_likes = mysqli_query($db,"UPDATE posts_tbl SET likes = '".$pre_likes."' WHERE id = '".$post_id."'");

			$get_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_like_status = mysqli_query($db, "SELECT * FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$like_status = mysqli_fetch_assoc($get_like_status);

			$likes = mysqli_fetch_assoc($get_likes);

			$validator['likes'] = $likes['likes'];
			$validator['success'] = true;
			$validator['like_status'] = $like_status;
			$validator['messages'] = "Disliked";
		} else {
			$dis_like = mysqli_query($db, "UPDATE liked_users SET liked = 1 WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$get_pre_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = ".$post_id."");

			$pre_likes_s = mysqli_fetch_assoc($get_pre_likes);

			$pre_likes = (int)$pre_likes_s['likes'];

			$pre_likes = $pre_likes +1;

			$update_likes = mysqli_query($db,"UPDATE posts_tbl SET likes = '".$pre_likes."' WHERE id = '".$post_id."'");

			$get_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = '".$post_id."'");

			$get_like_status = mysqli_query($db, "SELECT * FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$like_status = mysqli_fetch_assoc($get_like_status);

			$likes = mysqli_fetch_assoc($get_likes);

			$validator['likes'] = $likes['likes'];
			$validator['success'] = true;
			$validator['messages'] = "Liked";
			$validator['like_status'] = $like_status;
		}

	} else {

		$insert_liked = mysqli_query($db,"INSERT INTO liked_users (u_id, p_id) VALUES ('".$u_id."','".$post_id."')");

		$get_pre_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = ".$post_id."");

		$pre_likes_s = mysqli_fetch_assoc($get_pre_likes);

		$pre_likes = (int)$pre_likes_s['likes'];

		$pre_likes = $pre_likes +1;

		$update_likes = mysqli_query($db,"UPDATE posts_tbl SET likes = '".$pre_likes."' WHERE id = '".$post_id."'");

		$get_likes = mysqli_query($db, "SELECT likes FROM posts_tbl WHERE id = '".$post_id."'");

		$get_like_status = mysqli_query($db, "SELECT * FROM liked_users WHERE u_id = '".$u_id."' AND p_id = '".$post_id."'");

			$like_status = mysqli_fetch_assoc($get_like_status);

		$likes = mysqli_fetch_assoc($get_likes);

		$validator['success'] = true;
		$validator['likes'] = $likes['likes'];
		$validator['messages'] = "Liked";
		$validator['like_status'] = $like_status;
	}
}

echo json_encode($validator);


?>