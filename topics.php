
<?php include('header.php'); ?>
	    <div class="container">
        <div class="row">
	<div class="col-md-12 m-sm-0">
	<div class="content-body">
		<div class="col-md-3" id="posts_left_block">
			<?php include('left_blk.php'); ?>
		</div>
		<div class="col-md-6 topic-body m-sm-0" style="width: 53%;">
		    <p style="font-size: 25px;border-bottom: 1px solid #ccc;padding-bottom: 5px;"><b>Topics</b>
            </p>
            <div style="background-color: #fff;border: 1px solid #ccc;padding: 10px;">
	            <div style="float: right;">
	                <span id="sortby_date" style="font-size: 15px;cursor: pointer;">By Date</span>&nbsp;&nbsp;
	                <span id='sortby_alpha' class="sort_active" style="font-size: 15px;cursor: pointer;" onclick="getTopics()">A-Z</span>
	            </div><br>
			    <div class="topics">
	              <ul class="topics_ul" id="topics_ul">
	                  
	              </ul>
	            </div>
	        </div>
	    </div>
	    <div class="col-md-2" id="posts_right_block">right block ...</div>
        </div>
            </div>
            </div>
<?php include('footer.php'); ?>
<script type="text/javascript" src="./assets/js/check_user_session.js"></script>
<script type="text/javascript" src="./assets/js/topics.js"></script>

